From gradle:6.7-jdk11-openj9 AS grad
WORKDIR training_service
COPY  . .
RUN gradle build

FROM openjdk:15-slim AS builder
WORKDIR training_service
COPY --from=grad home/gradle/training_service/build/libs/*.jar statistic.jar
RUN java -Djarmode=layertools -jar statistic.jar extract


FROM openjdk:15-slim
WORKDIR training_service
RUN java -version
COPY --from=builder training_service/dependencies/ ./
COPY --from=builder training_service/spring-boot-loader/ ./
COPY --from=builder training_service/snapshot-dependencies/ ./
COPY --from=builder training_service/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]