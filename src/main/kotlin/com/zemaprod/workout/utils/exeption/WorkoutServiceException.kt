package com.zemaprod.workout.utils.exeption

import java.lang.RuntimeException

class WorkoutServiceException : RuntimeException {

    var errorCode = ErrorCodeDefinition.UNKNOWN_ERROR

    constructor(message: String, errorCode: ErrorCodeDefinition) : super(message) {
        this.errorCode = errorCode
    }

    constructor(message: String, errorCode: ErrorCodeDefinition, cause: Throwable) : super(message, cause) {
        this.errorCode = errorCode
    }
}
