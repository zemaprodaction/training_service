package com.zemaprod.workout.utils.exeption

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ZemaExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(Throwable::class)
    private fun handleUnknownExceptions(ex: Throwable): ResponseEntity<WorkoutServiceApiError> {
        return buildResponseEntity(WorkoutServiceApiError(ErrorCodeDefinition.UNKNOWN_ERROR, ErrorCodeDefinition.UNKNOWN_ERROR.httpStatus, ex.message
            ?: "Encountered unknown error, detailed message not provided"))
    }

    @ExceptionHandler(WorkoutServiceException::class)
    private fun handleWorkoutExceptions(ex: WorkoutServiceException): ResponseEntity<WorkoutServiceApiError> {
        return buildResponseEntity(WorkoutServiceApiError(ex.errorCode, ex.errorCode.httpStatus, ex.message ?: "message not provided"))
    }

    @ExceptionHandler(SecurityException::class)
    private fun handleSecurityExceptions(ex: Exception): ResponseEntity<WorkoutServiceApiError> {
        return buildResponseEntity(WorkoutServiceApiError(ErrorCodeDefinition.SECURITY_ERROR, ErrorCodeDefinition.SECURITY_ERROR.httpStatus, ex.message
            ?: "Encountered security error, detailed message not provided"))
    }

    private fun buildResponseEntity(workoutServiceApiError: WorkoutServiceApiError) =
        ResponseEntity(workoutServiceApiError, workoutServiceApiError.httpStatus)
}

data class WorkoutServiceApiError(
    val errorCode: ErrorCodeDefinition,
    val httpStatus: HttpStatus,
    val message: String
)
