package com.zemaprod.workout.utils.logger

import org.apache.logging.log4j.LogManager.getRootLogger

object LoggerManager {

    fun infoLog(message: String) {
        getRootLogger().info(message)
    }

    fun infoLog(message: String, vararg params: Any) {
        getRootLogger().info(message, params)
    }

    fun errorLog(message: String) {
        getRootLogger().error(message)
    }
}