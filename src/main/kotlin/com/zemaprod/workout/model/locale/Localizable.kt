package com.zemaprod.workout.model.locale

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document("localizable")
data class Localizable(
    @Id
    val id: String,
    val localizable: Map<String, String>
)

val DEFAULT_LANGUAGE: Locale = Locale.ENGLISH
