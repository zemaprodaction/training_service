package com.zemaprod.workout.model.exercise.measure

data class Measure(
    val expectedValue: Double = 0.0,
    val measureTypeId: String = ""
)
