package com.zemaprod.workout.model.exercise

import com.zemaprod.workout.model.entity_builder.AdditionalInfoBuilder
import com.zemaprod.workout.model.equipment.Equipment
import org.springframework.data.mongodb.core.mapping.DBRef

data class AdditionalInfo(
    @DBRef
    val targetMuscle: List<Muscle> = emptyList(),
    @DBRef
    val musclesInvolved: List<Muscle> = emptyList(),
    @DBRef
    val equipment: List<Equipment?> = emptyList()
) {
    companion object {
        inline fun additionalInfo(additionalInfo: AdditionalInfo = AdditionalInfo(), additionalInfoBuilder: AdditionalInfoBuilder.() -> Unit): AdditionalInfo {
            val builder = AdditionalInfoBuilder(additionalInfo)
            builder.additionalInfoBuilder()
            return builder.build()
        }
    }
}
