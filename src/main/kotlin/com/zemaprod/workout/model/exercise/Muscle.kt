package com.zemaprod.workout.model.exercise

import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.Document
import javax.persistence.Id

@Document
data class Muscle(
    @Id
    val id: String = ObjectId().toHexString(),
    val name: String = ""
)
