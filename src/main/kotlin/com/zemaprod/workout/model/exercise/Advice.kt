package com.zemaprod.workout.model.exercise

data class Advice(
    val listOfAdvice: List<String> = emptyList()
)
