package com.zemaprod.workout.model.exercise.measure

enum class MeasurePrefix(val prefix: String) {
    METER("meter"), COUNT("times"), KILO("kg"), SEC("sec")
}
