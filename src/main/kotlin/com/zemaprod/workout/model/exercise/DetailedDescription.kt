package com.zemaprod.workout.model.exercise

import com.zemaprod.workout.model.ImageBundle
import com.zemaprod.workout.model.entity_builder.DetailedDescriptionBuilder

data class DetailedDescription(
    val imageBundle: ImageBundle = ImageBundle(),
    val description: String = ""
) {
    companion object {
        inline fun detailedDescription(exerciseBuilder: DetailedDescriptionBuilder.() -> Unit): DetailedDescription {
            val builder = DetailedDescriptionBuilder()
            builder.exerciseBuilder()
            return builder.build()
        }
    }
}
