package com.zemaprod.workout.model.exercise

enum class ExerciseType {
    CARDIO, STRENGTH, FLEXIBILITY
}
