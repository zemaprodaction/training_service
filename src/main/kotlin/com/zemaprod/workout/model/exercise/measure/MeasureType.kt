package com.zemaprod.workout.model.exercise.measure

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("measureType")
data class MeasureType(
    @Id
    val id: String = ObjectId().toHexString(),
    val name: String = "",
    val measurePrefix: String,
    var measurePrefixLocalized: String
)
