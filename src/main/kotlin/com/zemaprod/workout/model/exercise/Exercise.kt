package com.zemaprod.workout.model.exercise

import com.zemaprod.workout.model.ImageBundle
import com.zemaprod.workout.model.Video
import com.zemaprod.workout.model.entity_builder.ExerciseBuilder
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("exercises")
data class Exercise(
    @Id
    val id: String = ObjectId().toHexString(),
    val name: String = "",
    val exerciseType: ExerciseType = ExerciseType.STRENGTH,
    val imageBundle: ImageBundle = ImageBundle(),
    val video: Video = Video(),
    val detailedDescription: String = "",
    val additionalInfo: AdditionalInfo = AdditionalInfo()
) {
    companion object {
        inline fun exercise(exercise: Exercise = Exercise(), exerciseBuilder: ExerciseBuilder.() -> Unit): Exercise {
            val builder = ExerciseBuilder(exercise)
            builder.exerciseBuilder()
            return builder.build()
        }
    }
}
