package com.zemaprod.workout.model.exercise

enum class ExerciseGroup(val group: String) {
    ABS("Abs"),
    BACK("Back"),
    CHEST("Chest"),
    GLUTES("Glutes"),
    LOWER_LEG("Lower leg"),
    UPPER_LEG("Upper leg"),
    SHOULDERS("Shoulders"),
    TRICEPS("Triceps"),
    BICEPS("Biceps"),
    NOT_SET("not set"),
}