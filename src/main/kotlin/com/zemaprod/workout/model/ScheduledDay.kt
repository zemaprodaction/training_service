package com.zemaprod.workout.model

import com.zemaprod.workout.model.workout.ScheduledWorkout
import java.time.LocalDate

data class ScheduledDay(
    val scheduledWorkoutList: List<ScheduledWorkout>,
    var actualDate: LocalDate? = null
)
