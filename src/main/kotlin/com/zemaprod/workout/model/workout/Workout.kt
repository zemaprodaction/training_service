package com.zemaprod.workout.model.workout

import com.zemaprod.workout.model.entity_builder.WorkoutBuilder
import com.zemaprod.workout.model.filter.Filterable
import com.zemaprod.workout.model.shared.WorkoutCommon
import com.zemaprod.workout.model.workout.workoutcharacteristic.WorkoutCharacteristic
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("workout")
data class Workout(
    @Id
    val id: String = ObjectId().toHexString(),
    val common: WorkoutCommon = WorkoutCommon(),
    val listOfPhases: List<WorkoutPhase> = ArrayList(),
    val duration: Long = 2700,
    @property:Filterable
    val characteristic: WorkoutCharacteristic = WorkoutCharacteristic()
) {
    companion object {
        inline fun workout(workout: Workout = Workout(), workoutBuilder: WorkoutBuilder.() -> Unit): Workout {
            val builder = WorkoutBuilder(workout)
            builder.workoutBuilder()
            return builder.build()
        }
    }

}
