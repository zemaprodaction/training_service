package com.zemaprod.workout.model.workout

enum class WorkoutProgramType {
    CARDIO, HEALTH, POWER
}
