package com.zemaprod.workout.model.workout

import kotlin.collections.ArrayList

data class WorkoutInstancePhase(
    var name: String = "",
    val listOfSets: List<WorkoutSet> = ArrayList(),
    val isOptional: Boolean = false
)
