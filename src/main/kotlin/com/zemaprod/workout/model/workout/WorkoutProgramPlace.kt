package com.zemaprod.workout.model.workout

enum class WorkoutProgramPlace {

    STREET, HOME, GYM

}
