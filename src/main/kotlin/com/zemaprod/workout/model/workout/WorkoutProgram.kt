package com.zemaprod.workout.model.workout

import com.fasterxml.jackson.annotation.JsonIgnore
import com.zemaprod.workout.model.entity_builder.WorkoutProgramBuilder
import com.zemaprod.workout.model.equipment.Equipment
import com.zemaprod.workout.model.filter.*
import com.zemaprod.workout.model.shared.Goal
import com.zemaprod.workout.model.shared.WorkoutCommon
import com.zemaprod.workout.model.workout.workout_cycle.WorkoutWeek
import com.zemaprod.workout.model.workout.workoutcharacteristic.WorkoutProgramCharacteristic
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.annotation.Transient
import java.util.*

@Document("workoutProgram")
data class WorkoutProgram(
    @Id
    val id: String = ObjectId().toHexString(),
    val common: WorkoutCommon = WorkoutCommon(),
    val listOfWeeks: List<WorkoutWeek> = ArrayList(),
    val isPublished: Boolean = false,
    @DBRef
    val goal: Goal = Goal(name = ""),
    @property:Filterable
    val characteristic: WorkoutProgramCharacteristic = WorkoutProgramCharacteristic()
) {


    @Transient
    var workoutsOnProgram: List<String> = emptyList()
        get() = listOfWeeks.asSequence()
            .flatMap { workoutWeek ->
                workoutWeek.listOfDays.asSequence()
                    .flatMap { workoutDay ->
                        workoutDay.listOfScheduledWorkouts.asSequence()
                            .map { it.workoutId }
                    }
            }.toList().distinct()
        private set

    @Transient
    var amountOfDays: Int = 0
        get() = listOfWeeks.sumBy { it.listOfDays.size }
        private set(value) {
            field = value
        }

    val amountWorkoutInWeek: IntRange
        get() = IntRange(listOfWeeks.asSequence().map { it.listOfDays.size }.asSequence().minOrNull() ?: 0,
            listOfWeeks.asSequence().map { it.listOfDays.size }.asSequence().maxOrNull() ?: 0)

    companion object {
        inline fun workoutProgram(program: WorkoutProgram = WorkoutProgram(), workoutBuilder: WorkoutProgramBuilder.() -> Unit): WorkoutProgram {
            val builder = WorkoutProgramBuilder(program)
            builder.workoutBuilder()
            return builder.build()
        }
    }

    fun populatePlaces(workouts: List<Workout>): WorkoutProgram = this.apply {
        val places = workouts.flatMap { workout ->
            workout.characteristic.workoutPlaces.value
        }
        this.characteristic.workoutPlaces.value = places
    }
}
