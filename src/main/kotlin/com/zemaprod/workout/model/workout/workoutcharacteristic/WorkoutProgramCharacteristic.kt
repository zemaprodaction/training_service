package com.zemaprod.workout.model.workout.workoutcharacteristic

import com.fasterxml.jackson.annotation.JsonIgnore
import com.zemaprod.workout.model.entity_builder.WorkoutCharacteristicBuilder
import com.zemaprod.workout.model.filter.*
import com.zemaprod.workout.model.workout.workout_cycle.WorkoutWeek
import com.zemaprod.workout.service.LocalizationService
import java.util.*

data class WorkoutProgramCharacteristic(
    override var workoutType: WorkoutType = WorkoutType(),
    override var workoutDifficulty: WorkoutDifficulty = WorkoutDifficulty(),
    override var equipmentNecessity: EquipmentNecessity = EquipmentNecessity(),
    @Transient
    var workoutPlaces: WorkoutPlace = WorkoutPlace().apply { editable = false },
    @get:JsonIgnore val listOfWeeks: List<WorkoutWeek> = emptyList(),
) : Characteristic() {

    companion object {
        inline fun characteristic(workoutCharacteristic: WorkoutProgramCharacteristic = WorkoutProgramCharacteristic(),
            workoutCharacteristicBuilder: WorkoutCharacteristicBuilder<WorkoutProgramCharacteristic>.() -> Unit): Characteristic {
            val builder = WorkoutCharacteristicBuilder(workoutCharacteristic)
            builder.workoutCharacteristicBuilder()
            return builder.build()
        }
    }

    override fun localized(localizationService: LocalizationService, locale: Locale) = copy(
        workoutPlaces = workoutPlaces.apply {
            name = localizationService.getString(name, locale)
            value = value.map { value -> localizationService.getString(value, locale) }
        },
    ).apply {
        super.localized(localizationService, locale)
    }
}
