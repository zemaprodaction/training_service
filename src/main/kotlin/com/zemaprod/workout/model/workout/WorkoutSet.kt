package com.zemaprod.workout.model.workout

import com.zemaprod.workout.model.exercise.measure.Measure

data class WorkoutSet(
    val listOfMeasures: List<Measure> = emptyList(),
    val exerciseId: String = "",
    val restTime: Int = 60,
    val workoutSchedulerId: String = ""
)
