package com.zemaprod.workout.model.workout.workoutcharacteristic

import com.fasterxml.jackson.annotation.JsonIgnore
import com.zemaprod.workout.model.entity_builder.WorkoutCharacteristicBuilder
import com.zemaprod.workout.model.filter.EquipmentNecessity
import com.zemaprod.workout.model.filter.WorkoutDifficulty
import com.zemaprod.workout.model.filter.WorkoutPlace
import com.zemaprod.workout.model.filter.WorkoutType
import com.zemaprod.workout.model.workout.workout_cycle.WorkoutWeek
import com.zemaprod.workout.service.LocalizationService
import java.util.*

data class WorkoutCharacteristic(
    override var workoutType: WorkoutType = WorkoutType(),
    override var workoutDifficulty: WorkoutDifficulty = WorkoutDifficulty(),
    override var equipmentNecessity: EquipmentNecessity = EquipmentNecessity(),
    var workoutPlaces: WorkoutPlace = WorkoutPlace(),
    @get:JsonIgnore val listOfWeeks: List<WorkoutWeek> = emptyList(),
) : Characteristic() {

    companion object {
        inline fun characteristic(workoutCharacteristic: WorkoutCharacteristic = WorkoutCharacteristic(),
            workoutCharacteristicBuilder: WorkoutCharacteristicBuilder<WorkoutCharacteristic>.() -> Unit): Characteristic {
            val builder = WorkoutCharacteristicBuilder(workoutCharacteristic)
            builder.workoutCharacteristicBuilder()
            return builder.build()
        }
    }

    override fun localized(localizationService: LocalizationService, locale: Locale) = copy(
        workoutPlaces = workoutPlaces.apply {
            name = localizationService.getString(name, locale)
            value = value.map { value -> localizationService.getString(value, locale) }
        },
    ).apply {
        super.localized(localizationService, locale)
    }
}
