package com.zemaprod.workout.model.workout.workout_cycle

import com.zemaprod.workout.model.workout.ScheduledWorkout

data class WorkoutDay(
    val dayIndex: Int = -1,
    val listOfScheduledWorkouts: List<ScheduledWorkout> = emptyList()
)
