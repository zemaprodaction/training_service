package com.zemaprod.workout.model.workout

data class WorkoutPhase(
    var name: String = "",
    val listOfExercises: List<String> = ArrayList(),
    val isOptional: Boolean = false
)
