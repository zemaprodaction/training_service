package com.zemaprod.workout.model.workout

import com.zemaprod.workout.model.entity_builder.ScheduledWorkoutBuilder
import com.zemaprod.workout.model.filter.WorkoutPlace
import org.bson.types.ObjectId

data class ScheduledWorkout(
    val id: String = ObjectId().toHexString(),
    var name: String = "",
    val workoutId: String = "",
    val workoutProgramId: String = "",
    var isFinished: Boolean = false,
    val listOfWorkoutPhases: List<WorkoutInstancePhase> = emptyList(),
) {
    companion object {
        inline fun scheduledWorkout(workout: ScheduledWorkout = ScheduledWorkout(),
            workoutBuilder: ScheduledWorkoutBuilder.() -> Unit): ScheduledWorkout {
            val builder = ScheduledWorkoutBuilder(workout)
            builder.workoutBuilder()
            return builder.build()
        }
    }
}
