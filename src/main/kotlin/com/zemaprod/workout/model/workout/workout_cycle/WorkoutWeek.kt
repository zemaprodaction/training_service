package com.zemaprod.workout.model.workout.workout_cycle

import com.zemaprod.workout.model.entity_builder.WorkoutWeekBuilder

data class WorkoutWeek(
    val listOfDays: List<WorkoutDay> = emptyList()
) {
    companion object {
        inline fun workoutWeek(program: WorkoutWeek = WorkoutWeek(), workoutWeekBuilder: WorkoutWeekBuilder.() -> Unit): WorkoutWeek {
            val builder = WorkoutWeekBuilder(program)
            builder.workoutWeekBuilder()
            return builder.build()
        }
    }
}
