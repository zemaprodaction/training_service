package com.zemaprod.workout.model.workout.workoutcharacteristic

import com.zemaprod.workout.model.filter.EquipmentNecessity
import com.zemaprod.workout.model.filter.WorkoutDifficulty
import com.zemaprod.workout.model.filter.WorkoutType
import com.zemaprod.workout.service.LocalizationService
import java.util.*

abstract class Characteristic {

    abstract var workoutType: WorkoutType
    abstract var workoutDifficulty: WorkoutDifficulty
    abstract var equipmentNecessity: EquipmentNecessity


    open fun localized(localizationService: LocalizationService, locale: Locale) = apply {
        workoutType = workoutType.apply {
            name = localizationService.getString(name, locale)
            value = localizationService.getString(nativeValue, locale)
        }
        workoutDifficulty = workoutDifficulty.apply {
            name = localizationService.getString(name, locale)
            value = localizationService.getString(nativeValue, locale)
        }
        equipmentNecessity = equipmentNecessity.apply {
            name = localizationService.getString(name, locale)
            value = localizationService.getString(nativeValue, locale)
        }
    }
}
