package com.zemaprod.workout.model.workoutproperty

abstract class SingleWorkoutProperty: WorkoutProperty() {
    abstract var value: String
    abstract var nativeValue: String
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SingleWorkoutProperty

        if (value != other.value) return false

        return true
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }
}
