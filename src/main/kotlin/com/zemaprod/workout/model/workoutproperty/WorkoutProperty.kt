package com.zemaprod.workout.model.workoutproperty

abstract class WorkoutProperty {
    abstract var name: String
    abstract val id: String
    abstract val editable: Boolean
}
