package com.zemaprod.workout.model.workoutproperty

abstract class ListWorkoutProperty: WorkoutProperty() {
    abstract var value: List<String>
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ListWorkoutProperty

        if (value != other.value) return false

        return true
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }

}
