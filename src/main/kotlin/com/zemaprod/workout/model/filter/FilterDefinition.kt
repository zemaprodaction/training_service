package com.zemaprod.workout.model.filter

data class FilterDefinition(
    val id: String,
    val name: String,
    val fieldName: String,
    var editable: Boolean,
)
