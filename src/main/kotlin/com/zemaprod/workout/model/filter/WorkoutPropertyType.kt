package com.zemaprod.workout.model.filter

import com.zemaprod.workout.model.workoutproperty.ListWorkoutProperty
import com.zemaprod.workout.model.workoutproperty.SingleWorkoutProperty

enum class WorkoutPropertyType {
    WORKOUT_TYPE {
        override val id: String
            get() = "workoutType"
        override val defaultName: String
            get() = "Workout type"

    },
    WORKOUT_PLACE {
        override val id: String
            get() = "workoutPlace"
        override val defaultName: String
            get() = "Workout place"
    },

    WORKOUT_DIFFICULTY {
        override val id: String
            get() = "workoutDifficulty"
        override val defaultName: String
            get() = "Workout difficulty"
    },

    EQUIPMENT_NECESSITY {
        override val id: String
            get() = "equipmentNecessity"
        override val defaultName: String
            get() = "Equipment necessity"
    };

    abstract val id: String
    abstract val defaultName: String
}

class WorkoutType : SingleWorkoutProperty() {
    override var name: String = WorkoutPropertyType.WORKOUT_TYPE.defaultName
    override val id: String
        get() = WorkoutPropertyType.WORKOUT_TYPE.id
    override val editable: Boolean
        get() = true
    override var value: String = ""
    override var nativeValue: String = ""

    enum class FilterValue(val valueString: String) {
        CARDIO("Cardio"),
        STRENGTH("Strength"),
        HEALTH("Health")
    }

}

class WorkoutPlace : ListWorkoutProperty() {
    override var name: String = WorkoutPropertyType.WORKOUT_PLACE.defaultName
    override val id: String
        get() = WorkoutPropertyType.WORKOUT_PLACE.id
    override var value: List<String> = emptyList()
    override var editable: Boolean = true

    enum class FilterValue(val valueString: String) {
        HOME("Home"),
        STREET("Street"),
        GYM("Gym")
    }
}

class WorkoutDifficulty : SingleWorkoutProperty() {
    override var name: String = WorkoutPropertyType.WORKOUT_DIFFICULTY.defaultName
    override val id: String
        get() = WorkoutPropertyType.WORKOUT_DIFFICULTY.id
    override var value: String = ""
    override var nativeValue: String = ""
    override val editable: Boolean
        get() = true

    enum class FilterValue(val valueString: String) {
        EASY("Easy"),
        AVERAGE("Average"),
        HARD("Hard")
    }
}

class EquipmentNecessity : SingleWorkoutProperty() {

    override var name: String = WorkoutPropertyType.EQUIPMENT_NECESSITY.defaultName
    override var nativeValue: String = ""
    override val id: String
        get() = WorkoutPropertyType.EQUIPMENT_NECESSITY.id
    override var value: String = ""
    override val editable: Boolean
        get() = true

    enum class FilterValue(val valueString: String) {
        REQUIRED("Required"),
        NOT_REQUIRED("Not required")
    }
}
