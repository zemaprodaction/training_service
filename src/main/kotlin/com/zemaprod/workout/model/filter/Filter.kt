package com.zemaprod.workout.model.filter

data class Filter(
    val id: String,
    val name: String,
    val filterPath: String,
    val values: List<String>,
    val nativeValues: List<String>,
    val editable: Boolean
)
