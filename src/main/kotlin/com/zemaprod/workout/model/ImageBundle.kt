package com.zemaprod.workout.model

data class ImageBundle(
    val webImageLink: String = "",
    val mobileImageLink: String = ""
)