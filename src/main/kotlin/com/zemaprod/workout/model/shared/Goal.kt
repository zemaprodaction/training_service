package com.zemaprod.workout.model.shared

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("goal")
data class Goal(
    @Id
    val id: String = ObjectId().toHexString(),
    var name: String = ""
)
