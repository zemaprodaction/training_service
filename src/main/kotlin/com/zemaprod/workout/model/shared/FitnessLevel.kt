package com.zemaprod.workout.model.shared

enum class FitnessLevel(val level: String) {
    BEGINNER("Beginner"),
    INTERMEDIATE("Intermediate"),
    PRO("Professional");
}

