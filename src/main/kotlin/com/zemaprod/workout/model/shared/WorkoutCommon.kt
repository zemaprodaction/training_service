package com.zemaprod.workout.model.shared

import com.zemaprod.workout.model.ImageBundle
import com.zemaprod.workout.service.LocalizationService
import java.util.*

data class WorkoutCommon(
    var name: String = "",
    var description: String = "",
    val imageBundle: ImageBundle = ImageBundle()
) {

    fun localized(localizationService: LocalizationService, locale: Locale) = copy(
        name = localizationService.getString(name, locale),
        description = localizationService.getString(description, locale)
    )
}
