package com.zemaprod.workout.model.entity_builder

import com.zemaprod.workout.ZemaDSL
import com.zemaprod.workout.model.workout.workout_cycle.WorkoutDay
import com.zemaprod.workout.model.workout.workout_cycle.WorkoutWeek

@ZemaDSL
class WorkoutWeekBuilder(workoutWeek: WorkoutWeek) {

    private var workoutWeek = workoutWeek.copy()

    fun addDay(day: WorkoutDay = WorkoutDay(), block: WorkoutDayBuilder.() -> WorkoutDayBuilder) = apply {
        val builder = WorkoutDayBuilder(day)
        builder.block()
        workoutWeek = workoutWeek.copy(listOfDays = workoutWeek.listOfDays.plus(builder.build()))
    }

    fun build() = workoutWeek
}
