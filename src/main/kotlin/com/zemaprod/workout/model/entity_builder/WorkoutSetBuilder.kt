package com.zemaprod.workout.model.entity_builder

import com.zemaprod.workout.ZemaDSL
import com.zemaprod.workout.model.exercise.Exercise
import com.zemaprod.workout.model.exercise.measure.Measure
import com.zemaprod.workout.model.workout.WorkoutSet

@ZemaDSL
class WorkoutSetBuilder(workoutSet: WorkoutSet) {

    var workoutSet = workoutSet.copy()

    fun measures(measures: List<Measure>) = apply {
        workoutSet = workoutSet.copy(listOfMeasures = measures)
    }

    fun exercise(id: String) = apply {
        workoutSet = workoutSet.copy(exerciseId = id)
    }

    fun exercise(exercise: Exercise) = apply {
        workoutSet = workoutSet.copy(exerciseId = exercise.id)
    }

    fun restTime(sec: Int) = apply {
        workoutSet = workoutSet.copy(restTime = sec)
    }

    fun scheduledWorkoutId(id: String) = apply {
        workoutSet = workoutSet.copy(workoutSchedulerId = id)
    }

    fun build() = workoutSet
}
