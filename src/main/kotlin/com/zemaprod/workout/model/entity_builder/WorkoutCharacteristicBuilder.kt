package com.zemaprod.workout.model.entity_builder

import com.zemaprod.workout.model.filter.EquipmentNecessity
import com.zemaprod.workout.model.filter.WorkoutDifficulty
import com.zemaprod.workout.model.filter.WorkoutPlace
import com.zemaprod.workout.model.filter.WorkoutType
import com.zemaprod.workout.model.workout.workoutcharacteristic.Characteristic
import com.zemaprod.workout.model.workout.workoutcharacteristic.WorkoutCharacteristic

class WorkoutCharacteristicBuilder<CHARACTERISTIC : Characteristic>(workoutCharacteristic: CHARACTERISTIC) {

    private var workoutCharacteristic = workoutCharacteristic

    fun type(workoutType: WorkoutType.FilterValue) = apply {
        workoutCharacteristic = workoutCharacteristic.apply {
            this.workoutType = WorkoutType().apply { nativeValue = workoutType.valueString }
        }
    }

    fun difficulty(difficulty: WorkoutDifficulty.FilterValue) = apply {
        workoutCharacteristic = workoutCharacteristic.apply {
            workoutDifficulty = WorkoutDifficulty().apply {
                nativeValue = difficulty.valueString
            }
        }
    }

    fun equipmentNecessity(equipmentNecessity: EquipmentNecessity.FilterValue) = apply {
        workoutCharacteristic = workoutCharacteristic.apply {
            this.equipmentNecessity = EquipmentNecessity().apply { nativeValue = equipmentNecessity.valueString }
        }
    }

    // TODO move it out of here, as it not typesafe fore now, and will throw ClassCastException if used for WorkoutProgram
    fun workoutPlaces(workoutPlaces: List<WorkoutPlace.FilterValue>) = apply {
        workoutCharacteristic = (workoutCharacteristic as WorkoutCharacteristic).apply {
            this.workoutPlaces = WorkoutPlace().apply { value = workoutPlaces.map { it.valueString } }
        } as CHARACTERISTIC
    }

    fun build() = workoutCharacteristic
}
