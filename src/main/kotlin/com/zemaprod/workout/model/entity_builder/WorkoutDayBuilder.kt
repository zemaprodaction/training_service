package com.zemaprod.workout.model.entity_builder

import com.zemaprod.workout.ZemaDSL
import com.zemaprod.workout.model.workout.ScheduledWorkout
import com.zemaprod.workout.model.workout.workout_cycle.WorkoutDay

@ZemaDSL
class WorkoutDayBuilder(workoutDay: WorkoutDay) {

    var workoutDay = workoutDay.copy()

    fun dayIndex(index: Int) = apply {
        workoutDay = workoutDay.copy(dayIndex = index)
    }

    fun addScheduledWorkout(workout: ScheduledWorkout = ScheduledWorkout(), block: ScheduledWorkoutBuilder.() -> ScheduledWorkoutBuilder) = apply {
        val builder = ScheduledWorkoutBuilder(workout)
        builder.block()
        workoutDay = workoutDay.copy(listOfScheduledWorkouts = workoutDay.listOfScheduledWorkouts.plus(builder.build()))
    }

    fun scheduledWorkouts(workouts: List<ScheduledWorkout>) = apply {
        workoutDay = workoutDay.copy(listOfScheduledWorkouts = workoutDay.listOfScheduledWorkouts.plus(workouts))
    }

    fun build() = workoutDay
}
