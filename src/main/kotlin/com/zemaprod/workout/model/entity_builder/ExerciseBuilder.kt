package com.zemaprod.workout.model.entity_builder

import com.zemaprod.workout.ZemaDSL
import com.zemaprod.workout.model.ImageBundle
import com.zemaprod.workout.model.Video
import com.zemaprod.workout.model.exercise.*

@ZemaDSL
class ExerciseBuilder(val exercise: Exercise) {

    private var exerciseToBuild = exercise.copy()

    fun id(id: String) = apply {
        exerciseToBuild = exerciseToBuild.copy(id = id)
    }

    fun name(name: String) = apply {
        exerciseToBuild = exerciseToBuild.copy(name = name)
    }

    fun exerciseType(exerciseType: ExerciseType) = apply {
        exerciseToBuild = exerciseToBuild.copy(exerciseType = exerciseType)
    }

    fun images(block: () -> ImageBundle) = apply {
        exerciseToBuild = exerciseToBuild.copy(imageBundle = block())
    }

    fun video(block: () -> Video) = apply {
        exerciseToBuild = exerciseToBuild.copy(video = block())
    }

    fun detailDescription(detailedDescription: String) = apply {
        exerciseToBuild = exerciseToBuild.copy(detailedDescription = detailedDescription)
    }

    fun additionalInfo(block: AdditionalInfoBuilder.() -> AdditionalInfoBuilder) = apply {
        val builder = AdditionalInfoBuilder(exerciseToBuild.additionalInfo)
        builder.block()
        exerciseToBuild = exerciseToBuild.copy(
            additionalInfo = builder.build()
        )
    }

    fun build() = exerciseToBuild
}
