package com.zemaprod.workout.model.entity_builder

import com.zemaprod.workout.ZemaDSL
import com.zemaprod.workout.model.ImageBundle
import com.zemaprod.workout.model.equipment.Equipment

@ZemaDSL
class EquipmentBuilder(val equipment: Equipment) {

    private var equipmentToBuild = equipment.copy()

    fun id(id: String) = apply {
        equipmentToBuild = equipmentToBuild.copy(id = id)
    }

    fun name(name: String) = apply {
        equipmentToBuild = equipmentToBuild.copy(name = name)
    }

    fun build() = equipmentToBuild
}
