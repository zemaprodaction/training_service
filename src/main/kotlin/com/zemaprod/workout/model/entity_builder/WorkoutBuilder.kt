package com.zemaprod.workout.model.entity_builder

import com.zemaprod.workout.model.ImageBundle
import com.zemaprod.workout.model.shared.WorkoutCommon
import com.zemaprod.workout.model.workout.Workout
import com.zemaprod.workout.model.workout.workoutcharacteristic.WorkoutCharacteristic
import com.zemaprod.workout.model.workout.WorkoutPhase

class WorkoutBuilder(workout: Workout) {

    var workout = workout.copy()

    fun id(id: String) = apply {
        workout = workout.copy(id = id)
    }

    fun name(name: String) = apply {
        workout = workout.copy(common = workout.common.copy(name = name))
    }

    fun description(description: String) = apply {
        workout = workout.copy(common = workout.common.copy(description = description))
    }

    fun images(images: () -> ImageBundle) = apply {
        workout = workout.copy(common = workout.common.copy(imageBundle = images()))
    }

    fun addPhase(phase: WorkoutPhase = WorkoutPhase(), block: WorkoutPhaseBuilder.() -> WorkoutPhaseBuilder) = apply {
        val builder = WorkoutPhaseBuilder(phase)
        builder.block()
        workout = workout.copy(listOfPhases = workout.listOfPhases.plus(builder.build()))
    }

    fun characteristics(characteristic: WorkoutCharacteristic = WorkoutCharacteristic(),
        block: WorkoutCharacteristicBuilder<WorkoutCharacteristic>.() -> WorkoutCharacteristicBuilder<WorkoutCharacteristic>) =
        apply {
            val builder = WorkoutCharacteristicBuilder(characteristic)
            builder.block()
            workout = workout.copy(characteristic = builder.build())
        }

    fun characteristics(characteristic: WorkoutCharacteristic) = apply {
        workout = workout.copy(characteristic = characteristic)
    }

    fun workoutPhases(phases: List<WorkoutPhase>) = apply {
        workout = workout.copy(listOfPhases = phases)
    }

    fun common(workoutCommon: WorkoutCommon) = apply {
        workout = workout.copy(common = workoutCommon)
    }

    fun duration(duration: Long) = apply {
        workout = workout.copy(duration = duration)
    }

    fun build() = workout
}
