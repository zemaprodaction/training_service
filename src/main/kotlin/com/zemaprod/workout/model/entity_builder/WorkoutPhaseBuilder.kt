package com.zemaprod.workout.model.entity_builder

import com.zemaprod.workout.model.exercise.Exercise
import com.zemaprod.workout.model.workout.WorkoutPhase

class WorkoutPhaseBuilder(phase: WorkoutPhase) {

    private var phase = phase.copy()

    fun name(name: String) = apply {
        phase = phase.copy(name = name)
    }

    @JvmName("exerciseIds")
    fun exercises(exerciseIds: List<String>) = apply {
        phase = phase.copy(listOfExercises = exerciseIds)
    }

    fun exercises(exercises: List<Exercise>) = apply {
        phase = phase.copy(listOfExercises = exercises.map { it.id })
    }

    fun build() = phase
}
