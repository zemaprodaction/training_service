package com.zemaprod.workout.model.entity_builder

import com.zemaprod.workout.ZemaDSL
import com.zemaprod.workout.model.ImageBundle
import com.zemaprod.workout.model.shared.Goal
import com.zemaprod.workout.model.workout.WorkoutProgram
import com.zemaprod.workout.model.workout.workout_cycle.WorkoutWeek
import com.zemaprod.workout.model.workout.workoutcharacteristic.WorkoutProgramCharacteristic

@ZemaDSL
class WorkoutProgramBuilder(workoutProgram: WorkoutProgram) {

    var program = workoutProgram.copy()

    fun id(id: String) = apply {
        program = program.copy(id = id)
    }

    fun name(name: String) = apply {
        program = program.copy(common = program.common.copy(name = name))
    }

    fun description(description: String) = apply {
        program = program.copy(common = program.common.copy(description = description))
    }

    fun images(images: () -> ImageBundle) = apply {
        program = program.copy(common = program.common.copy(imageBundle = images()))
    }

    fun characteristics(characteristic: WorkoutProgramCharacteristic = WorkoutProgramCharacteristic(),
        block: WorkoutCharacteristicBuilder<WorkoutProgramCharacteristic>.() -> WorkoutCharacteristicBuilder<WorkoutProgramCharacteristic>) =
        apply {
            val builder = WorkoutCharacteristicBuilder(characteristic)
            builder.block()
            program = program.copy(characteristic = builder.build())
        }

    fun addWeek(week: WorkoutWeek = WorkoutWeek(), block: WorkoutWeekBuilder.() -> WorkoutWeekBuilder) = apply {
        val builder = WorkoutWeekBuilder(week)
        builder.block()
        program = program.copy(listOfWeeks = program.listOfWeeks.plus(builder.build()))
    }

    fun goal(goal: Goal) = apply {
        program = program.copy(goal = goal)
    }

    fun build() = program
}
