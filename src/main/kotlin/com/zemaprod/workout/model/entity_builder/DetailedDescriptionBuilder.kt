package com.zemaprod.workout.model.entity_builder

import com.zemaprod.workout.ZemaDSL
import com.zemaprod.workout.model.ImageBundle
import com.zemaprod.workout.model.exercise.DetailedDescription

@ZemaDSL
class DetailedDescriptionBuilder {

    private var detailsDescription = DetailedDescription()

    fun images(block: () -> ImageBundle) = apply {
        detailsDescription = detailsDescription.copy(imageBundle = block())
    }

    fun description(description: String) = apply {
        detailsDescription = detailsDescription.copy(description = description)
    }

    fun build() = detailsDescription
}
