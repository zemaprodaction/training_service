package com.zemaprod.workout.model.entity_builder

import com.zemaprod.workout.ZemaDSL
import com.zemaprod.workout.model.ImageBundle
import com.zemaprod.workout.model.equipment.Equipment
import com.zemaprod.workout.model.exercise.AdditionalInfo
import com.zemaprod.workout.model.exercise.Muscle

@ZemaDSL
class AdditionalInfoBuilder(additionalInfo: AdditionalInfo) {

    private var additionalInfo = additionalInfo.copy()

    fun targetMuscle(muscles: List<Muscle>) = apply {
        additionalInfo = additionalInfo.copy(targetMuscle = muscles)
    }

    fun involvedMuscles(muscles: List<Muscle>) = apply {
        additionalInfo = additionalInfo.copy(musclesInvolved = muscles)
    }

    fun addEquipment(equipment: Equipment) = apply {
        additionalInfo = additionalInfo.copy(equipment = additionalInfo.equipment.plus(equipment))
    }

    fun equipment(equipment: List<Equipment?>) = apply {
        additionalInfo = additionalInfo.copy(equipment = equipment)
    }

    fun build() = additionalInfo
}
