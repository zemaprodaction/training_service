package com.zemaprod.workout.model.entity_builder

import com.zemaprod.workout.ZemaDSL
import com.zemaprod.workout.model.workout.WorkoutInstancePhase
import com.zemaprod.workout.model.workout.WorkoutSet

@ZemaDSL
class WorkoutInstancePhaseBuilder(workoutInstancePhase: WorkoutInstancePhase) {

    private var workoutInstancePhase = workoutInstancePhase.copy()

    fun name(name: String) = apply {
        workoutInstancePhase = workoutInstancePhase.copy(name = name)
    }

    fun addWorkoutSet(set: WorkoutSet = WorkoutSet(), block: WorkoutSetBuilder.() -> WorkoutSetBuilder) = apply {
        val builder = WorkoutSetBuilder(set)
        builder.block()
        workoutInstancePhase = workoutInstancePhase.copy(listOfSets = workoutInstancePhase.listOfSets.plus(builder.build()))
    }

    fun build() = workoutInstancePhase
}
