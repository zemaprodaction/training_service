package com.zemaprod.workout.model.entity_builder

import com.zemaprod.workout.ZemaDSL
import com.zemaprod.workout.model.workout.*

@ZemaDSL
class ScheduledWorkoutBuilder(scheduledWorkout: ScheduledWorkout) {

    var scheduledWorkout = scheduledWorkout.copy()

    fun id(id: String) = apply {
        scheduledWorkout = scheduledWorkout.copy(id = id)
    }

    fun name(name: String) = apply {
        scheduledWorkout = scheduledWorkout.copy(name = name)
    }

    fun workout(id: String) = apply {
        scheduledWorkout = scheduledWorkout.copy(workoutId = id)
    }

    fun workout(workout: Workout) = apply {
        scheduledWorkout = scheduledWorkout.copy(workoutId = workout.id)
    }

    fun workoutProgram(id: String) = apply {
        scheduledWorkout = scheduledWorkout.copy(workoutProgramId = id)
    }

    fun workout(workoutProgram: WorkoutProgram) = apply {
        scheduledWorkout = scheduledWorkout.copy(workoutProgramId = workoutProgram.id)
    }

    fun listOfWorkoutPhases(phases: List<WorkoutInstancePhase>) = apply {
        scheduledWorkout = scheduledWorkout.copy(listOfWorkoutPhases = phases)
    }

    fun addWorkoutPhase(phase: WorkoutInstancePhase = WorkoutInstancePhase(), block: WorkoutInstancePhaseBuilder.() -> WorkoutInstancePhaseBuilder) =
        apply {
            val builder = WorkoutInstancePhaseBuilder(phase)
            builder.block()
            scheduledWorkout = scheduledWorkout.copy(listOfWorkoutPhases = scheduledWorkout.listOfWorkoutPhases.plus(builder.build()))
        }

    fun build() = scheduledWorkout
}
