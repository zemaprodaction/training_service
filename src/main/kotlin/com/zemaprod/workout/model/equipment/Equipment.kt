package com.zemaprod.workout.model.equipment

import com.zemaprod.workout.model.entity_builder.EquipmentBuilder
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("equipment")
data class Equipment(
    @Id
    val id: String = ObjectId().toHexString(),
    val name: String = "",
) {
    companion object {
        inline fun equipment(equipment: Equipment = Equipment(), equipmentBuilder: EquipmentBuilder.() -> Unit): Equipment {
            val builder = EquipmentBuilder(equipment)
            builder.equipmentBuilder()
            return builder.build()
        }
    }
}
