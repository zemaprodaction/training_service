package com.zemaprod.workout.model

data class Video(
    val url: String = ""
)
