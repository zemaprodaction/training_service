package com.zemaprod.workout.repository

import com.zemaprod.workout.model.shared.Goal
import org.springframework.stereotype.Repository

@Repository
interface GoalRepository : ZemaMongoRepository<Goal, String>
