package com.zemaprod.workout.repository

import com.zemaprod.workout.model.exercise.Muscle
import org.springframework.stereotype.Repository

@Repository
interface MusclesRepository : ZemaMongoRepository<Muscle, String> {

}
