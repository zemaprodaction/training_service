package com.zemaprod.workout.repository

import com.zemaprod.workout.model.exercise.Exercise
import org.springframework.stereotype.Repository

@Repository
interface ExerciseRepository : ZemaMongoRepository<Exercise, String> {

    fun findByIdIsIn(ids: Set<String>): List<Exercise>
}
