package com.zemaprod.workout.repository

import com.zemaprod.workout.model.filter.*
import com.zemaprod.workout.model.workout.WorkoutProgram
import org.springframework.stereotype.Repository
import kotlin.reflect.KClass
import kotlin.reflect.jvm.jvmName

@Repository
class FilterWorkoutProgramInMemoryRepository : FilterRepository {

    private val workoutPlaceFilter: WorkoutPlace = WorkoutPlace()
    private val workoutTypeFilter: WorkoutType = WorkoutType()
    private val workoutDifficultyFilter: WorkoutDifficulty = WorkoutDifficulty()
    private val equipmentNecessity: EquipmentNecessity = EquipmentNecessity()
    private val storeOfFilter = mapOf(
        workoutPlaceFilter.id to listOf(
            WorkoutPlace.FilterValue.HOME.valueString,
            WorkoutPlace.FilterValue.STREET.valueString,
            WorkoutPlace.FilterValue.GYM.valueString
        ),
        workoutTypeFilter.id to listOf(
            WorkoutType.FilterValue.CARDIO.valueString,
            WorkoutType.FilterValue.STRENGTH.valueString,
            WorkoutType.FilterValue.HEALTH.valueString
        ),
        workoutDifficultyFilter.id to listOf(
            WorkoutDifficulty.FilterValue.EASY.valueString,
            WorkoutDifficulty.FilterValue.AVERAGE.valueString,
            WorkoutDifficulty.FilterValue.HARD.valueString
        ),
        equipmentNecessity.id to listOf(
            EquipmentNecessity.FilterValue.REQUIRED.valueString,
            EquipmentNecessity.FilterValue.NOT_REQUIRED.valueString
        ),
    )
    private lateinit var filterList: Map<String, List<FilterDefinition>>

    override fun saveFilters(filterList: Map<String, List<FilterDefinition>>): Map<String, List<FilterDefinition>> {
        if (this::filterList.isInitialized) {
            return filterList
        }
        this.filterList = filterList
        return filterList
    }

    override fun findAllFilterValueByType(filterId: String) = storeOfFilter[filterId] ?: emptyList()

    override fun findAllFilterNativeValueByType(filterId: String) = storeOfFilter[filterId] ?: emptyList()

    override fun <T : Any> findFilterDefinitionByClass(filterForClass: KClass<T>): List<FilterDefinition> {
        return filterList[filterForClass.jvmName]?.apply {
            if (filterForClass == WorkoutProgram::class) {
                this.find { it.id == WorkoutPlace().id }?.editable = false
            }
        } ?: emptyList()
    }

}
