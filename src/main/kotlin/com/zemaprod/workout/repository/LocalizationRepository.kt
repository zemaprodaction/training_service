package com.zemaprod.workout.repository

import com.zemaprod.workout.model.locale.Localizable
import org.springframework.stereotype.Repository

@Repository
interface LocalizationRepository : ZemaMongoRepository<Localizable, String>
