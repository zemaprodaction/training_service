package com.zemaprod.workout.repository

import com.zemaprod.workout.model.workout.Workout
import org.springframework.stereotype.Repository

@Repository
interface WorkoutRepository : ZemaMongoRepository<Workout, String> {

    fun findByIdIsIn(ids: Set<String>): List<Workout>
}
