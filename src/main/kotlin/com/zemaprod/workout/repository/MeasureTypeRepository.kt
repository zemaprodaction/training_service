package com.zemaprod.workout.repository

import com.zemaprod.workout.model.exercise.measure.MeasureType
import org.springframework.stereotype.Repository

@Repository
interface MeasureTypeRepository : ZemaMongoRepository<MeasureType, String>
