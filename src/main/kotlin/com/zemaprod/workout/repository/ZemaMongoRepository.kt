package com.zemaprod.workout.repository

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.repository.NoRepositoryBean
import java.io.Serializable

@NoRepositoryBean
interface ZemaMongoRepository<T, ID : Serializable> : MongoRepository<T, ID>
