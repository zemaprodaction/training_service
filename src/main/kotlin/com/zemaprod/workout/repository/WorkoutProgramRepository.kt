package com.zemaprod.workout.repository

import com.zemaprod.workout.model.workout.WorkoutProgram
import org.springframework.stereotype.Repository

@Repository
interface WorkoutProgramRepository : ZemaMongoRepository<WorkoutProgram, String> {
}
