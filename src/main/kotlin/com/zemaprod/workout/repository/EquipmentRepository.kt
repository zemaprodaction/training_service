package com.zemaprod.workout.repository

import com.zemaprod.workout.model.equipment.Equipment
import org.springframework.stereotype.Repository

@Repository
interface EquipmentRepository : ZemaMongoRepository<Equipment, String>
