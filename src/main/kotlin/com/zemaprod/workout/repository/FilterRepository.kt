package com.zemaprod.workout.repository

import com.zemaprod.workout.model.filter.Filter
import com.zemaprod.workout.model.filter.FilterDefinition
import kotlin.reflect.KClass

interface FilterRepository {

    fun saveFilters(filterList: Map<String, List<FilterDefinition>>): Map<String, List<FilterDefinition>>

    fun findAllFilterValueByType(filterId: String): List<String>

    fun findAllFilterNativeValueByType(filterId: String): List<String>

    fun <T: Any> findFilterDefinitionByClass(filterForClass: KClass<T>): List<FilterDefinition>
}
