package com.zemaprod.workout.controller

import com.zemaprod.workout.model.workout.Workout
import com.zemaprod.workout.service.WorkoutService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/workout")
class WorkoutController(
    private val workoutService: WorkoutService
) {
    @GetMapping
    fun getAllWorkouts(locale: Locale): List<Workout> = workoutService.findAll(locale)

    @GetMapping("{id}")
    fun getWorkoutById(
        locale: Locale,
        @PathVariable(value = "id") id: String
    ): Workout = workoutService.findById(locale, id)

    @PostMapping("/workouts")
    fun getWorkoutsByIds(
        locale: Locale,
        @RequestBody ids: Set<String>
    ): List<Workout> = workoutService.findByIdIsIn(locale, ids = ids)

    @PostMapping
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    fun createWorkoutAdmin(
        locale: Locale,
        @RequestBody workout: Workout
    ): Workout = workoutService.saveWorkout(locale, workout)

    @PutMapping
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    fun updateWorkoutAdmin(
        locale: Locale,
        @RequestBody workout: Workout
    ): Workout = workoutService.saveWorkout(locale, workout)

    @DeleteMapping("{id}")
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    fun deleteWorkout(@PathVariable(value = "id") id: String): ResponseEntity<String> {
        workoutService.removeWorkout(id)
        return ResponseEntity.ok().build()
    }
}
