package com.zemaprod.workout.controller

import com.zemaprod.workout.model.locale.Localizable
import com.zemaprod.workout.service.LocalizationService
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/localization")
class LocalizationController(
    private val localizationService: LocalizationService
) {

    @GetMapping("{string}")
    fun getString(
        locale: Locale,
        @PathVariable(value = "string") string: String
    ): String = localizationService.getString(string, locale)

    @GetMapping("{string}/{lang}")
    fun getStringForSpecificLang(
        @PathVariable(value = "string") string: String,
        @PathVariable(value = "lang") lang: Locale
    ): String = localizationService.getString(string, lang)

    @GetMapping
    fun getAllStrings(
    ): List<Localizable> = localizationService.getAllStrings()

    @PutMapping
    fun localize(
        locale: Locale,
        @RequestBody stringsMap: Map<String, Map<Locale, String>>
    ): Localizable = localizationService.localize(stringsMap)
}
