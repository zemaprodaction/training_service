package com.zemaprod.workout.controller

import com.zemaprod.workout.model.exercise.Exercise
import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import com.zemaprod.workout.service.ExerciseService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/exercise")
class ExerciseController(
    private val exerciseService: ExerciseService
) {

    @GetMapping
    fun getAllExercises(locale: Locale): List<Exercise> = exerciseService.findAllExercises(locale)

    @GetMapping("{id}")
    fun getExerciseById(locale: Locale, @PathVariable(value = "id") id: String): Exercise = exerciseService.findById(locale, id)

    @PostMapping
    fun createExercise(locale: Locale, @RequestBody exercise: Exercise): Exercise = exerciseService.saveExercise(DEFAULT_LANGUAGE, exercise)

    @PostMapping("/exercises")
    fun getExerciseByIds(locale: Locale, @RequestBody exercisesIds: Set<String>): List<Exercise> = exerciseService.findByIdIsIn(locale, ids = exercisesIds)

    @PutMapping
    fun updateExercise(locale: Locale, @RequestBody exercise: Exercise): Exercise = exerciseService.saveExercise(DEFAULT_LANGUAGE, exercise)

    @DeleteMapping("{id}")
    fun deleteExercise(@PathVariable(value = "id") id: String): ResponseEntity<String> {
        exerciseService.removeExercise(id)
        return ResponseEntity.ok().build()
    }
}
