package com.zemaprod.workout.controller

import com.zemaprod.workout.model.filter.Filter
import com.zemaprod.workout.model.workout.Workout
import com.zemaprod.workout.model.workout.WorkoutProgram
import com.zemaprod.workout.service.FilterService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/filter")
class FilterController(
    val filterService: FilterService
) {

    @GetMapping("/workout_program")
    fun getAllWorkoutProgramFilters(
        locale: Locale
    ): List<Filter> {
        return filterService.getAllFilterForEntity(locale, WorkoutProgram::class)
    }

    @GetMapping("/workout")
    fun getAllWorkoutFilters(
        locale: Locale
    ): List<Filter> {
        return filterService.getAllFilterForEntity(locale, Workout::class)
    }
}
