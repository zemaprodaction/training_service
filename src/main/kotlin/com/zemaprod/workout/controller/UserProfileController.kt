package com.zemaprod.workout.controller

import com.zemaprod.workout.service.UserService
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal


@RestController
@RequestMapping("/user_profile")
class UserProfileController(
    private val userService: UserService
) {

    @GetMapping("me")
    fun getMe(principal: Principal?) = userService.extractUserFromJwtClaims(principal)

}
