package com.zemaprod.workout.controller

import com.zemaprod.workout.model.workout.WorkoutProgram
import com.zemaprod.workout.service.WorkoutProgramService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.security.Principal
import java.util.*

@RestController
@RequestMapping("/workoutProgram")
class WorkoutProgramController(
    private val workoutProgramService: WorkoutProgramService,
) {

    @GetMapping
    fun getAllWorkoutPrograms(locale: Locale, principal: Principal?): List<WorkoutProgram> {
        return workoutProgramService.findAll(locale, principal)
    }

    @GetMapping("{id}")
    fun getWorkoutProgramById(
        locale: Locale,
        @PathVariable(value = "id") id: String
    ): WorkoutProgram = workoutProgramService.findById(locale, id)

    @PostMapping
    fun createWorkoutProgram(
        locale: Locale,
        @RequestBody workoutProgram: WorkoutProgram
    ): WorkoutProgram = workoutProgramService.saveWorkoutProgram(locale, workoutProgram)

    @PutMapping
    fun updateWorkoutProgram(
        locale: Locale,
        @RequestBody workoutProgram: WorkoutProgram
    ): WorkoutProgram = workoutProgramService.saveWorkoutProgram(locale, workoutProgram)

    @DeleteMapping("{id}")
    fun deleteWorkoutProgram(
        @PathVariable(value = "id") id: String
    ): ResponseEntity<String> {
        workoutProgramService.removeWorkoutProgram(id)
        return ResponseEntity.ok().build()
    }

    @GetMapping("/my")
    fun getUserWorkoutPrograms(
        locale: Locale,
        principal: Principal
    ): List<WorkoutProgram> = workoutProgramService.findAll(locale, principal)
}
