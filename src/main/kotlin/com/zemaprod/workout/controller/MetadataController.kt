package com.zemaprod.workout.controller

import com.zemaprod.workout.model.equipment.Equipment
import com.zemaprod.workout.model.exercise.Muscle
import com.zemaprod.workout.model.exercise.measure.MeasureType
import com.zemaprod.workout.model.shared.Goal
import com.zemaprod.workout.service.*
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/metadata")
class MetadataController(
    val goalService: GoalService,
    val equipmentService: EquipmentService,
    val measureTypeService: MeasureTypeService,
    val muscleService: MuscleService,
    val localizationService: LocalizationService
) {

    @GetMapping("goal")
    fun getAllGoals(locale: Locale): List<Goal> = goalService.finaAll(locale)

    @GetMapping("goal/{id}")
    fun getGoalById(locale: Locale, @PathVariable("id") id: String): Goal = goalService.finaById(locale, id)

    @GetMapping("equipment")
    fun getAllEquipment(locale: Locale) = equipmentService.findAll(locale)

    @PostMapping("equipment")
    fun saveEquipment(locale: Locale, @RequestBody equipment: Equipment) = equipmentService.saveEquipment(locale, equipment)

    @DeleteMapping("equipment/{id}")
    fun deleteEquipment(@PathVariable(value = "id") id: String) = equipmentService.removeById(id)

    @GetMapping("measureTypes")
    fun getAllMeasures(locale: Locale): List<MeasureType> = measureTypeService.findAll(locale)

    @GetMapping("muscles")
    fun getAllMuscles(locale: Locale): List<Muscle> = muscleService.findAll()

    @PostMapping("muscles")
    fun createMuscles(locale: Locale, @RequestBody muscle: Muscle): Muscle = muscleService.save(locale, muscle)

    @DeleteMapping("muscles/{id}")
    fun deleteMuscles(@PathVariable(value = "id") id: String) = muscleService.remove(id)

}
