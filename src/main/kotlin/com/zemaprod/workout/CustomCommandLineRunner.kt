package com.zemaprod.workout

import com.zemaprod.workout.model.workout.Workout
import com.zemaprod.workout.model.workout.WorkoutProgram
import com.zemaprod.workout.service.FilterService
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component

@Component
class CustomCommandLineRunner(val filterService: FilterService) : CommandLineRunner {

    override fun run(vararg args: String?) {
        filterService.initFilters(listOf(WorkoutProgram::class, Workout::class))
    }

}
