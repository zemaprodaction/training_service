package com.zemaprod.workout.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.web.SecurityFilterChain

@Configuration
class OAuth2ResourceServerSecurityConfiguration {

    @Bean
    fun securityFilterChain(http: HttpSecurity): SecurityFilterChain
    {
        http.authorizeRequests {
            it.anyRequest().permitAll().and().csrf().disable()
        }
            .oauth2ResourceServer().jwt()
        return http.build()
    }
}
