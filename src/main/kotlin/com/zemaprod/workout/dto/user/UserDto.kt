package com.zemaprod.workout.dto.user

data class UserDto(
    val name: String,
    val imageUrl: String = "none avatar",
    val isAuthorized: Boolean = false
)

