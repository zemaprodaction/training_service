package com.zemaprod.workout.service

import com.zemaprod.workout.model.entity_builder.ExerciseBuilder
import com.zemaprod.workout.model.exercise.Exercise
import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import java.util.*

interface ExerciseService {

    fun findAllExercises(locale: Locale = DEFAULT_LANGUAGE): List<Exercise>

    fun findById(locale: Locale = DEFAULT_LANGUAGE, id: String): Exercise

    fun saveExercise(lang: Locale = DEFAULT_LANGUAGE, exercise: Exercise): Exercise

    fun saveExercise(lang: Locale = DEFAULT_LANGUAGE, exercise: Exercise = Exercise(), exerciseBuilder: ExerciseBuilder.() -> Unit): Exercise

    fun removeExercise(id: String)

    fun findByIdIsIn(locale: Locale = DEFAULT_LANGUAGE, ids: Set<String>): List<Exercise>
}
