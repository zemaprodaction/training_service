package com.zemaprod.workout.service

import com.zemaprod.workout.model.entity_builder.WorkoutBuilder
import com.zemaprod.workout.model.workout.Workout
import com.zemaprod.workout.model.workout.Workout.Companion.workout
import com.zemaprod.workout.repository.WorkoutRepository
import com.zemaprod.workout.utils.exeption.ErrorCodeDefinition
import com.zemaprod.workout.utils.exeption.WorkoutServiceException
import org.springframework.stereotype.Service
import java.util.*

@Service
class WorkoutServiceImpl(
    val workoutRepository: WorkoutRepository,
    val localizationService: LocalizationService
) : WorkoutService {

    override fun saveWorkout(locale: Locale, workout: Workout): Workout {
        localizationService.run {
            addString(locale, workout.common.name)
            addString(locale, workout.common.description)
            workout.listOfPhases.forEach { phase ->
                addString(locale, phase.name)
            }
        }
        return workoutRepository.save(workout).localized(locale)
    }

    override fun saveWorkout(locale: Locale, workout: Workout, workoutBuilder: WorkoutBuilder.() -> Unit): Workout {
        return saveWorkout(locale, Workout.workout(workout, workoutBuilder))
    }

    override fun findAll(locale: Locale): List<Workout> = workoutRepository.findAll().map { it.localized(locale) }

    override fun findById(locale: Locale, id: String): Workout = workoutRepository.findById(id).orElseThrow {
        WorkoutServiceException("Workout with id \"$id\" was not found", ErrorCodeDefinition.WORKOUT_NOT_FOUND)
    }.localized(locale)

    override fun findByIdsIn(locale: Locale, ids: Set<String>): List<Workout> = workoutRepository.findByIdIsIn(ids).map { it.localized(locale) }

    override fun deleteAll() {
        workoutRepository.deleteAll()
    }

    override fun removeWorkout(id: String) {
        workoutRepository.deleteById(id)
    }

    override fun findByIdIsIn(locale: Locale, ids: Set<String>): List<Workout> = workoutRepository.findByIdIsIn(ids).map { it.localized(locale) }

    private fun Workout.localized(locale: Locale): Workout = workout(this) {
        characteristics(characteristic.localized(localizationService, locale))
        common(common.localized(localizationService, locale))
        workoutPhases(listOfPhases.map { it.copy(name = localizationService.getString(it.name, locale)) })
    }
}
