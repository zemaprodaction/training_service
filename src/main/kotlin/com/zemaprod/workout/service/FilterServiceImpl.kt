package com.zemaprod.workout.service

import com.fasterxml.jackson.annotation.JsonIgnore
import com.zemaprod.workout.model.filter.Filter
import com.zemaprod.workout.model.filter.FilterDefinition
import com.zemaprod.workout.model.filter.Filterable
import com.zemaprod.workout.model.workoutproperty.WorkoutProperty
import com.zemaprod.workout.repository.FilterRepository
import org.springframework.stereotype.Service
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.full.*
import kotlin.reflect.jvm.jvmName

@Service
class FilterServiceImpl(
    private val filterRepository: FilterRepository,
    private val localizationService: LocalizationService,
) : FilterService {

    override fun initFilters(filtersForEntity: List<KClass<out Any>>): Map<String, List<FilterDefinition>> {
        val resultMap = mutableMapOf<String, List<FilterDefinition>>()
        filtersForEntity.forEach {
            val resultList = convertFieldToFilters(it)
            resultMap[it.jvmName] = resultList
        }
        filterRepository.saveFilters(resultMap)
        return resultMap
    }

    override fun <T : Any> getAllFilterForEntity(locale: Locale, entityClass: KClass<T>): List<Filter> {
        val filterDefinitionList = filterRepository.findFilterDefinitionByClass(entityClass)
        return filterDefinitionList.map {
            val values = filterRepository.findAllFilterValueByType(it.id).map { filterValue -> localizationService.getString(filterValue, locale) }
            val nativeValues = filterRepository.findAllFilterValueByType(it.id)
            Filter(it.id, localizationService.getString(it.name, locale), it.fieldName, values, nativeValues, it.editable)
        }
    }

    private fun <T : Any> convertFieldToFilters(c: KClass<T>): List<FilterDefinition> {
        return c.declaredMemberProperties
            .filter {
                it.hasAnnotation<Filterable>()
            }
            .flatMap {
                val parentProperties = ((it.returnType.classifier as KClass<*>).allSuperclasses as List).filter {
                    !it.isInstance(Any::class)
                }.flatMap { it.declaredMemberProperties }
                val allProperties = parentProperties + (it.returnType.classifier as KClass<*>).declaredMemberProperties
                allProperties
                    .filter {
                        !it.getter.hasAnnotation<JsonIgnore>()
                    }
                    .map {
                        val entity = (it.returnType.classifier as KClass<*>).primaryConstructor?.call() as WorkoutProperty
                        FilterDefinition(entity.id, entity.name, it.name, entity.editable)
                    }
            }.toSet().toList()
    }
}
