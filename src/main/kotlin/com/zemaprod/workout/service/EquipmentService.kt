package com.zemaprod.workout.service

import com.zemaprod.workout.model.entity_builder.EquipmentBuilder
import com.zemaprod.workout.model.equipment.Equipment
import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import java.util.*

interface EquipmentService {

    fun findAll(locale: Locale = DEFAULT_LANGUAGE): List<Equipment>

    fun findById(locale: Locale = DEFAULT_LANGUAGE, id: String): Equipment

    fun saveEquipment(locale: Locale = DEFAULT_LANGUAGE, equipment: Equipment): Equipment

    fun saveEquipment(locale: Locale = DEFAULT_LANGUAGE, equipment: Equipment = Equipment(), equipmentBuilder: EquipmentBuilder.() -> Unit): Equipment

    fun removeById(id: String)
}
