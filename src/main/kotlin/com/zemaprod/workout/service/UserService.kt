package com.zemaprod.workout.service

import com.zemaprod.workout.dto.user.UserDto
import java.security.Principal

interface UserService {

    fun extractUserFromJwtClaims(principal: Principal?): UserDto

    fun isAdmin(principal: Principal?): Boolean
}
