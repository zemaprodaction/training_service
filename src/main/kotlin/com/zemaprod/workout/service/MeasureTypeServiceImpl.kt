package com.zemaprod.workout.service

import com.zemaprod.workout.model.exercise.measure.MeasureType
import com.zemaprod.workout.repository.MeasureTypeRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class MeasureTypeServiceImpl(
    val measureTypeRepository: MeasureTypeRepository,
    val localizationService: LocalizationService,
) : MeasureTypeService {

    override fun findAll(locale: Locale): List<MeasureType> = measureTypeRepository.findAll().map { it.localize(locale) }

    private fun MeasureType.localize(locale: Locale) = this.copy(
        name = localizationService.getString(name, locale),
        measurePrefixLocalized = localizationService.getString(measurePrefix, locale)
    )
}
