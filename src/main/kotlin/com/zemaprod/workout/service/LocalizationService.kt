package com.zemaprod.workout.service

import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import com.zemaprod.workout.model.locale.Localizable
import java.util.*

interface LocalizationService {

    fun addString(locale: Locale = DEFAULT_LANGUAGE, string: String)

    fun localize(locale: Locale, stringsMap: Map<String, String>, updateStringsCache: Boolean = true): Localizable

    fun localize(stringsMap: Map<String, Map<Locale, String>>): Localizable

    fun getString(key: String, locale: Locale = DEFAULT_LANGUAGE): String

    fun getAllStrings(): List<Localizable>
}
