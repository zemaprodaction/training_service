package com.zemaprod.workout.service

import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import com.zemaprod.workout.model.locale.Localizable
import com.zemaprod.workout.repository.LocalizationRepository
import com.zemaprod.workout.utils.exeption.ErrorCodeDefinition
import com.zemaprod.workout.utils.exeption.WorkoutServiceException
import org.springframework.stereotype.Service
import java.security.MessageDigest
import java.util.*

@Service
class LocalizationServiceImpl(
    val localizationRepository: LocalizationRepository
) : LocalizationService {

    init {
        updateStringsCache()
    }

    private lateinit var allStrings: List<Localizable>

    override fun addString(locale: Locale, string: String) {
        if (allStrings.none { it.localizable[locale.language] == string }) {
            localizationRepository.save(Localizable(string.sha256(), mapOf(locale.language to string))).also {
                updateStringsCache()
            }
        }
    }

    override fun localize(locale: Locale, stringsMap: Map<String, String>, updateStringsCache: Boolean): Localizable {
        val id = stringsMap.keys.first().sha256()

        val fieldToUpdate: Map<String, String> = allStrings.find { it.id == id }?.localizable
            ?: throw WorkoutServiceException("String with id \"$id\" was not found", ErrorCodeDefinition.LOCALIZATION_ERROR)

        if (locale.language == DEFAULT_LANGUAGE.language &&
            fieldToUpdate[DEFAULT_LANGUAGE.language] != null &&
            stringsMap.values.first() != fieldToUpdate[DEFAULT_LANGUAGE.language]
        ) {
            throw WorkoutServiceException("It is not allowed to localize string of default ($DEFAULT_LANGUAGE) language", ErrorCodeDefinition.LOCALIZATION_ERROR)
        }

        localizationRepository.delete(Localizable(id, fieldToUpdate))

        return localizationRepository.save(
            Localizable(fieldToUpdate.values.first().sha256(), fieldToUpdate.plus(mapOf(locale.language to stringsMap.values.first())))
        ).also { updateStringsCache() }
    }

    override fun localize(stringsMap: Map<String, Map<Locale, String>>): Localizable {
        val id: String = stringsMap.keys.first()
        stringsMap.forEach { map ->
            map.getLocales().forEachIndexed { index, locale ->

                if (locale == DEFAULT_LANGUAGE && !isStringAlreadyAdded(map.getLocalizableStringByIndex(index))) {
                    // adding string for En language
                    addString(locale, map.getLocalizableStringByIndex(index))
                    // localizing the original locale with en string
                    localize(locale, mapOf(id to map.getLocalizableStringByIndex(index)), false)
                    // localizing the en locale with all the passed languages
                    stringsMap.values.forEach { localizationMap ->
                        localize(mapOf(map.getLocalizableStringByIndex(index) to localizationMap))
                    }
                    // adding original locale localization to en locale
                    val originalLocalizable = getAllStrings().find { it.id == stringsMap.keys.first().sha256() }?.localizable!!.map {
                        mapOf(Locale.forLanguageTag(it.key) to it.value)
                    }.first()
                    localize(mapOf(map.getLocalizableStringByIndex(index) to originalLocalizable))

                } else localize(locale, mapOf(id to map.getLocalizableStringByIndex(index)), false)
            }
        }
        updateStringsCache()
        val hashedKey = id.sha256()
        return allStrings.find { it.id == hashedKey }
            ?: throw WorkoutServiceException("Localization was not successful", ErrorCodeDefinition.LOCALIZATION_ERROR)
    }

    override fun getString(key: String, locale: Locale): String {
        val id = key.sha256()
        for (localizable in allStrings) {
            return localizable.localizable[locale.language].takeIf { localizable.id == id } ?: continue
        }
        return key
    }

    override fun getAllStrings(): List<Localizable> {
        return allStrings
    }

    private fun updateStringsCache() {
        allStrings = localizationRepository.findAll()
    }

    private fun String.sha256(): String {
        val digest = MessageDigest.getInstance("SHA-256")
        val hash = digest.digest(this.toByteArray(charset("UTF-8")))
        val hexString = StringBuilder()
        for (aHash in hash) {
            val hex = Integer.toHexString(0xff and aHash.toInt())
            if (hex.length == 1) hexString.append('0')
            hexString.append(hex)
        }
        return hexString.toString().toLowerCase()
    }

    private fun Map.Entry<String, Map<Locale, String>>.getLocalizableStringByIndex(index: Int) = value.values.toList()[index]
    private fun Map.Entry<String, Map<Locale, String>>.getLocales() = value.keys
    private fun isStringAlreadyAdded(string: String) = allStrings.any { it.id == string.sha256() }

}
