package com.zemaprod.workout.service

import com.zemaprod.workout.model.entity_builder.WorkoutBuilder
import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import com.zemaprod.workout.model.workout.Workout
import java.util.*

interface WorkoutService {

    fun deleteAll()

    fun findAll(locale: Locale = DEFAULT_LANGUAGE): List<Workout>

    fun findById(locale: Locale = DEFAULT_LANGUAGE, id: String): Workout

    fun findByIdsIn(locale: Locale = DEFAULT_LANGUAGE, ids: Set<String>): List<Workout>

    fun saveWorkout(locale: Locale = DEFAULT_LANGUAGE, workout: Workout): Workout

    fun saveWorkout(locale: Locale = DEFAULT_LANGUAGE, workout: Workout = Workout(), workoutBuilder: WorkoutBuilder.() -> Unit): Workout

    fun removeWorkout(id: String)

    fun findByIdIsIn(locale: Locale = DEFAULT_LANGUAGE,ids: Set<String>): List<Workout>
}
