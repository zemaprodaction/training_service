package com.zemaprod.workout.service

import com.zemaprod.workout.dto.user.UserDto
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.security.Principal

@Service
@Transactional
class UserServiceImpl(
    val localizationService: LocalizationService,
) : UserService {


    private val defaultImage = "https://www.w3schools.com/howto/img_avatar.png"
    val imageUrl: String = "imageUrl"

    override fun extractUserFromJwtClaims(principal: Principal?): UserDto {
        return if (principal != null) {
            val claims = getClaimsFromOAuth2Principal(principal)
            val imageUrl = if (claims[imageUrl] == null) defaultImage else claims[imageUrl] as String
            UserDto(claims["preferred_username"] as String, imageUrl, true)
        } else {
            UserDto("anonimus", defaultImage)
        }
    }

    override fun isAdmin(principal: Principal?): Boolean {
        return if (principal != null) {
            val claims = getClaimsFromOAuth2Principal(principal)
            val imageUrl = if (claims[imageUrl] == null) defaultImage else claims[imageUrl] as String
            UserDto(claims["preferred_username"] as String, imageUrl, true)
            (claims["scope"] as String).contains("admin_panel", true)
        } else {
            false
        }
    }

    private fun getClaimsFromOAuth2Principal(principal: Principal): Map<*, *> {
        val oauth2Principal = principal as JwtAuthenticationToken
        val details = oauth2Principal.principal as Jwt
        return details.claims
    }

}


