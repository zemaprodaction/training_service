package com.zemaprod.workout.service

import com.zemaprod.workout.model.ScheduledDay
import com.zemaprod.workout.model.workout.ScheduledWorkout
import com.zemaprod.workout.model.workout.WorkoutProgram
import java.time.LocalDate

interface SchedulerService {

    fun createSchedulerByWorkoutProgram(workoutProgram: WorkoutProgram): Map<LocalDate, ScheduledDay>
}
