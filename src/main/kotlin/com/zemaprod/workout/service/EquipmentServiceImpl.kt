package com.zemaprod.workout.service

import com.zemaprod.workout.model.entity_builder.EquipmentBuilder
import com.zemaprod.workout.model.equipment.Equipment
import com.zemaprod.workout.model.equipment.Equipment.Companion.equipment
import com.zemaprod.workout.repository.EquipmentRepository
import com.zemaprod.workout.utils.exeption.ErrorCodeDefinition
import com.zemaprod.workout.utils.exeption.WorkoutServiceException
import org.springframework.stereotype.Service
import java.util.*

@Service
class EquipmentServiceImpl(
    private val equipmentRepository: EquipmentRepository,
    private val localizationService: LocalizationService
) : EquipmentService {

    override fun findAll(locale: Locale): List<Equipment> = equipmentRepository.findAll().map { it.localized(locale) }

    override fun findById(locale: Locale, id: String): Equipment {
        return equipmentRepository.findById(id).orElseThrow {
            throw WorkoutServiceException("Equipment with id \"$id\" not found", ErrorCodeDefinition.EQUIPMENT_NOT_FOUND)
        }
    }

    override fun saveEquipment(locale: Locale, equipment: Equipment): Equipment {
        localizationService.addString(locale, equipment.name)
        return equipmentRepository.save(equipment).localized(locale)
    }

    override fun saveEquipment(locale: Locale, equipment: Equipment, equipmentBuilder: EquipmentBuilder.() -> Unit): Equipment {
        return saveEquipment(locale, equipment(equipment, equipmentBuilder))
    }

    override fun removeById(id: String) {
        equipmentRepository.deleteById(id)
    }

    private fun Equipment.localized(locale: Locale): Equipment = equipment(this) {
        name(localizationService.getString(name, locale))
    }
}
