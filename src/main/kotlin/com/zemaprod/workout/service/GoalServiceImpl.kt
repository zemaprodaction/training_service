package com.zemaprod.workout.service

import com.zemaprod.workout.model.shared.Goal
import com.zemaprod.workout.repository.GoalRepository
import com.zemaprod.workout.utils.exeption.ErrorCodeDefinition
import com.zemaprod.workout.utils.exeption.WorkoutServiceException
import org.springframework.stereotype.Service
import java.util.*

@Service
class GoalServiceImpl(
    val goalRepository: GoalRepository,
    val localizationService: LocalizationService
) : GoalService {

    override fun finaAll(locale: Locale) = goalRepository.findAll().map { it.localize(locale) }

    override fun saveGoal(locale: Locale, goal: Goal): Goal {
        localizationService.addString(locale, goal.name)
        return goalRepository.save(goal).localize(locale)
    }

    override fun finaById(locale: Locale, id: String) = goalRepository.findById(id).orElseThrow {
        WorkoutServiceException("Goal by id $id does not exist", ErrorCodeDefinition.GOAL_NOT_FOUND)
    }.localize(locale)

    private fun Goal.localize(locale: Locale) = this.copy(name = localizationService.getString(name, locale))
}
