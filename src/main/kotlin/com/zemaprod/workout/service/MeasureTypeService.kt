package com.zemaprod.workout.service

import com.zemaprod.workout.model.exercise.measure.MeasureType
import java.util.*

interface MeasureTypeService {

    fun findAll(locale: Locale): List<MeasureType>
}
