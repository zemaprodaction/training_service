package com.zemaprod.workout.service

import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import com.zemaprod.workout.model.shared.Goal
import java.util.*

interface GoalService {

    fun finaAll(locale: Locale = DEFAULT_LANGUAGE): List<Goal>

    fun saveGoal(locale: Locale = DEFAULT_LANGUAGE, goal: Goal): Goal

    fun finaById(locale: Locale = DEFAULT_LANGUAGE, id: String): Goal
}
