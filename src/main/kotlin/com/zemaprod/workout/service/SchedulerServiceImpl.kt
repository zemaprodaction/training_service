package com.zemaprod.workout.service

import com.zemaprod.workout.model.ScheduledDay
import com.zemaprod.workout.model.workout.ScheduledWorkout
import com.zemaprod.workout.model.workout.WorkoutProgram
import org.springframework.stereotype.Service
import java.lang.Math.abs
import java.time.LocalDate

@Service
class SchedulerServiceImpl : SchedulerService {
    override fun createSchedulerByWorkoutProgram(workoutProgram: WorkoutProgram): Map<LocalDate, ScheduledDay> {
        val today: LocalDate = LocalDate.now()
        val schedulerMap = mutableMapOf<LocalDate, ScheduledDay>()
        val firstDay = workoutProgram.listOfWeeks[0].listOfDays[0]
        val prevDayIndex = firstDay.dayIndex
        schedulerMap[today] = ScheduledDay(scheduledWorkoutList = firstDay.listOfScheduledWorkouts)
        workoutProgram.listOfWeeks.forEachIndexed { index, workoutWeek ->
            workoutWeek.listOfDays.forEachIndexed { dayIndex, day ->
                if (index != 0 || dayIndex != 0) {
                    val plusDate = index * 7 + kotlin.math.abs(day.dayIndex - prevDayIndex)
                    val dateOfWorkout = today.plusDays(plusDate.toLong())
                    schedulerMap[dateOfWorkout] = ScheduledDay(day.listOfScheduledWorkouts)
                }
            }
        }
        return schedulerMap
    }
}
