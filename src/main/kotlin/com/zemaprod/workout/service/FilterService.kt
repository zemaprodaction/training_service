package com.zemaprod.workout.service

import com.zemaprod.workout.model.filter.Filter
import com.zemaprod.workout.model.filter.FilterDefinition
import java.util.*
import kotlin.reflect.KClass

interface FilterService {

    fun  initFilters(filtersForEntity: List<KClass<out Any>>): Map<String, List<FilterDefinition>>

    fun <T: Any> getAllFilterForEntity(locale: Locale, entityClass: KClass<T>): List<Filter>

}
