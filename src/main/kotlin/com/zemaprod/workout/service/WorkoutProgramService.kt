package com.zemaprod.workout.service

import com.zemaprod.workout.model.entity_builder.WorkoutProgramBuilder
import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import com.zemaprod.workout.model.workout.WorkoutProgram
import java.security.Principal
import java.util.*

interface WorkoutProgramService {

    fun findAll(locale: Locale = DEFAULT_LANGUAGE, principal: Principal?): List<WorkoutProgram>

    fun findById(locale: Locale = DEFAULT_LANGUAGE, id: String): WorkoutProgram

    fun removeWorkoutProgram(id: String)

    fun deleteAll()

    fun saveWorkoutProgram(locale: Locale = DEFAULT_LANGUAGE, workoutProgram: WorkoutProgram): WorkoutProgram

    fun saveWorkoutProgram(locale: Locale = DEFAULT_LANGUAGE, workoutProgram: WorkoutProgram = WorkoutProgram(), workoutProgramBuilder: WorkoutProgramBuilder.() -> Unit): WorkoutProgram

    fun localized(locale: Locale, workoutProgram: WorkoutProgram): WorkoutProgram
}
