package com.zemaprod.workout.service

import com.zemaprod.workout.model.exercise.Muscle
import com.zemaprod.workout.repository.MusclesRepository
import com.zemaprod.workout.utils.exeption.ErrorCodeDefinition
import com.zemaprod.workout.utils.exeption.WorkoutServiceException
import org.springframework.stereotype.Service
import java.util.*

@Service
class MuscleServiceImpl(val repository: MusclesRepository, val localizationService: LocalizationService) : MuscleService {

    override fun save(lang: Locale, muscle: Muscle): Muscle {
        localizationService.run {
            addString(lang, muscle.name)
        }
        return repository.save(muscle)
    }

    override fun findAll(): List<Muscle> = repository.findAll()
    override fun remove(id: String) {
        this.repository.deleteById(id)
    }

    override fun findById(muscleId: String): Muscle {
        return repository.findById(muscleId).orElseThrow {
            throw WorkoutServiceException("Muscle not found", ErrorCodeDefinition.MUSCLE_NOT_FOUND)
        }
    }

}
