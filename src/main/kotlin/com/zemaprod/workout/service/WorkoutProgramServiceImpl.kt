package com.zemaprod.workout.service

import com.zemaprod.workout.model.entity_builder.WorkoutProgramBuilder
import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import com.zemaprod.workout.model.workout.WorkoutProgram
import com.zemaprod.workout.repository.WorkoutProgramRepository
import com.zemaprod.workout.utils.exeption.ErrorCodeDefinition
import com.zemaprod.workout.utils.exeption.WorkoutServiceException
import org.springframework.stereotype.Service
import java.security.Principal
import java.util.*
import kotlin.collections.ArrayList

@Service
class WorkoutProgramServiceImpl(
    val workoutProgramRepository: WorkoutProgramRepository,
    val workoutService: WorkoutService,
    val exerciseService: ExerciseService,
    val localizationService: LocalizationService,
    val userService: UserService,
) : WorkoutProgramService {

    override fun findAll(locale: Locale, principal: Principal?): List<WorkoutProgram> {
        return if (userService.isAdmin(principal)) {
            workoutProgramRepository.findAll().map { it.localized(locale).setDynamicFields().localizePlaces(locale) }
        } else {
            return workoutProgramRepository.findAll().filter { it.isPublished }.map { it.localized(locale).setDynamicFields().localizePlaces(locale) }
        }
    }

    override fun findById(locale: Locale, id: String): WorkoutProgram {
        return workoutProgramRepository.findById(id).orElseThrow {
            WorkoutServiceException("Workout program with id \"$id\" was not found", ErrorCodeDefinition.WORKOUT_PROGRAM_NOT_FOUND)
        }.localized(locale).setDynamicFields().localizePlaces(locale)
    }

    // refactor later
    private fun WorkoutProgram.localizePlaces(locale: Locale) = this.apply {
        characteristic.workoutPlaces.value = characteristic.workoutPlaces.value.map { localizationService.getString(it, locale) }
    }

    override fun removeWorkoutProgram(id: String) {
        workoutProgramRepository.deleteById(id)
    }

    override fun deleteAll() {
        workoutProgramRepository.deleteAll()
    }

    override fun saveWorkoutProgram(locale: Locale, workoutProgram: WorkoutProgram): WorkoutProgram {
        var workoutsOnProgramIds: List<String> = ArrayList()
        localizationService.run {
            addString(locale, workoutProgram.common.name)
            addString(locale, workoutProgram.common.description)
            workoutProgram.listOfWeeks.forEach { week ->
                week.listOfDays.forEach { day ->
                    day.listOfScheduledWorkouts.forEach { shceduled ->
                        workoutsOnProgramIds = workoutsOnProgramIds + shceduled.workoutId
                        addString(locale, shceduled.name)
                        shceduled.listOfWorkoutPhases.forEach { phas ->
                            addString(locale, phas.name)
                        }
                    }
                }
            }
        }
        val workoutsOnProgram = workoutService.findByIdIsIn(locale, workoutsOnProgramIds.toSet())
        return workoutProgramRepository.save(workoutProgram.apply { populatePlaces(workoutsOnProgram) }).localized(locale)
    }

    override fun saveWorkoutProgram(locale: Locale, workoutProgram: WorkoutProgram, workoutProgramBuilder: WorkoutProgramBuilder.() -> Unit): WorkoutProgram {
        return saveWorkoutProgram(locale, WorkoutProgram.workoutProgram(workoutProgram, workoutProgramBuilder))
    }

    override fun localized(locale: Locale, workoutProgram: WorkoutProgram): WorkoutProgram = workoutProgram.localized(locale)

//    private fun postProcessWorkoutProgramList(workoutProgramList: List<WorkoutProgram>, locale: Locale): List<WorkoutProgram> {
//        return workoutProgramList.map {
//            val workoutForWorkoutProgram = workoutService.findByIdsIn(locale, it.workoutsOnProgram.toSet())
//            it.populatePlaces(workoutForWorkoutProgram)
//        }
//    }
//
//    private fun postProcessWorkoutProgram(workoutProgram: WorkoutProgram, locale: Locale): WorkoutProgram {
//        val workoutForWorkoutProgram = workoutService.findByIdsIn(locale, workoutProgram.workoutsOnProgram.toSet())
//        return workoutProgram.populatePlaces(workoutForWorkoutProgram)
//    }


    private fun WorkoutProgram.localized(locale: Locale): WorkoutProgram = this.copy(
        characteristic = characteristic.localized(localizationService, locale),
        common = common.localized(localizationService, locale),
        listOfWeeks = listOfWeeks.map { week ->
            week.copy(listOfDays = week.listOfDays.map { day ->
                day.copy(listOfScheduledWorkouts = day.listOfScheduledWorkouts.map { sh ->
                    sh.copy(name = localizationService.getString(sh.name, locale), listOfWorkoutPhases = sh.listOfWorkoutPhases.map { phase ->
                        phase.copy(name = localizationService.getString(phase.name, locale))
                    })
                })
            })
        },
        goal = goal.copy(name = localizationService.getString(goal.name, locale))
    )

    private fun WorkoutProgram.setDynamicFields() = this.apply {
        val workoutsOnProgramIds =
            listOfWeeks.map { week ->
                week.listOfDays.map { day -> day.listOfScheduledWorkouts.map { workout -> workout.workoutId } }
            }.flatMap { it.toSet() }.flatMap { it.toSet() }.toSet()
        val workouts = workoutService.findByIdsIn(DEFAULT_LANGUAGE, workoutsOnProgramIds)
        val exercises = exerciseService.findByIdIsIn(DEFAULT_LANGUAGE, workouts
            .map { wk -> wk.listOfPhases.map { phase -> phase.listOfExercises }.flatMap { ex -> ex.toSet() } }
            .flatMap { it.toSet() }
            .toSet())
//        equipment = exercises.map { it.additionalInfo.equipment }.flatMap { it.toSet() }
        populatePlaces(workouts)
    }
}
