package com.zemaprod.workout.service

import com.zemaprod.workout.model.entity_builder.ExerciseBuilder
import com.zemaprod.workout.model.exercise.Exercise
import com.zemaprod.workout.model.exercise.Exercise.Companion.exercise
import com.zemaprod.workout.repository.ExerciseRepository
import com.zemaprod.workout.utils.exeption.ErrorCodeDefinition
import com.zemaprod.workout.utils.exeption.WorkoutServiceException
import org.springframework.stereotype.Service
import java.util.*

@Service
class ExerciseServiceImpl(
    val exerciseRepository: ExerciseRepository,
    val localizationService: LocalizationService
) : ExerciseService {

    override fun saveExercise(lang: Locale, exercise: Exercise): Exercise {
        localizationService.run {
            addString(lang, exercise.name)
            addString(lang, exercise.detailedDescription)
        }
        return exerciseRepository.save(exercise)
    }

    override fun saveExercise(lang: Locale, exercise: Exercise, exerciseBuilder: ExerciseBuilder.() -> Unit): Exercise {
        return saveExercise(lang, exercise(exercise, exerciseBuilder))
    }

    override fun findAllExercises(locale: Locale): MutableList<Exercise> = exerciseRepository.findAll().map { it.localized(locale) }.toMutableList()

    override fun findById(locale: Locale, id: String): Exercise {
        return exerciseRepository.findById(id).orElseThrow {
            WorkoutServiceException("Exercise with id $id does not exist", ErrorCodeDefinition.EXERCISE_NOT_FOUND)
        }.localized(locale)
    }

    override fun removeExercise(id: String) {
        exerciseRepository.deleteById(id)
    }

    override fun findByIdIsIn(locale: Locale, ids: Set<String>): List<Exercise> = exerciseRepository.findByIdIsIn(ids).map { it.localized(locale) }

    private fun Exercise.localized(locale: Locale): Exercise = exercise(this) {
        name(localizationService.getString(name, locale))
        detailDescription(localizationService.getString(detailedDescription, locale))
        additionalInfo {
            equipment(additionalInfo.equipment.map { it?.copy(name = localizationService.getString(it.name, locale)) })
            targetMuscle(additionalInfo.targetMuscle.map { it.copy(name = localizationService.getString(it.name, locale)) })
            involvedMuscles(additionalInfo.musclesInvolved.map { it.copy(name = localizationService.getString(it.name, locale)) })
        }
    }
}
