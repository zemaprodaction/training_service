package com.zemaprod.workout.service

import com.zemaprod.workout.model.exercise.Muscle
import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import java.util.*

interface MuscleService {

    fun save(lang: Locale = DEFAULT_LANGUAGE, muscle: Muscle): Muscle

    fun findById(muscleId: String): Muscle

    fun findAll(): List<Muscle>

    fun remove(id: String)
}
