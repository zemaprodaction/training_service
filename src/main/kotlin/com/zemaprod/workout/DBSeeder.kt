package com.zemaprod.workout

import com.zemaprod.workout.model.ImageBundle
import com.zemaprod.workout.model.Video
import com.zemaprod.workout.model.exercise.Muscle
import com.zemaprod.workout.model.exercise.measure.Measure
import com.zemaprod.workout.model.exercise.measure.MeasurePrefix
import com.zemaprod.workout.model.exercise.measure.MeasureType
import com.zemaprod.workout.model.filter.EquipmentNecessity
import com.zemaprod.workout.model.filter.WorkoutDifficulty
import com.zemaprod.workout.model.filter.WorkoutPlace
import com.zemaprod.workout.model.filter.WorkoutType
import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import com.zemaprod.workout.model.shared.Goal
import com.zemaprod.workout.model.workout.Workout
import com.zemaprod.workout.model.workout.Workout.Companion.workout
import com.zemaprod.workout.repository.*
import com.zemaprod.workout.service.*
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.util.*

@Component
@Profile("dev")
@ComponentScan("com.zemaprod.workout.service", "com.zemaprod.workout.repository")
class DBSeeder(
    val goalRepository: GoalRepository,
    val workoutService: WorkoutService,
    val exerciseService: ExerciseService,
    val exerciseRepository: ExerciseRepository,
    val workoutProgramService: WorkoutProgramService,
    val equipmentRepository: EquipmentRepository,
    val equipmentService: EquipmentService,
    val measureTypeRepository: MeasureTypeRepository,
    val localizationRepository: LocalizationRepository,
    val localizationService: LocalizationService,
    val musclesRepository: MusclesRepository,
) : CommandLineRunner {

    fun String.addString(locale: Locale = DEFAULT_LANGUAGE): String {
        localizationService.addString(locale, this)
        return this
    }

    fun String.localize(locale: Locale, value: String): String {
        localizationService.localize(locale, mapOf(this to value))
        return this
    }

    fun RUSSIAN(): Locale = Locale.forLanguageTag("ru")

    override fun run(vararg args: String?) {

        "Workout type".addString().localize(RUSSIAN(), "Тип тренировки")
        "Workout place".addString().localize(RUSSIAN(), "Место тренировки")
        "Workout difficulty".addString().localize(RUSSIAN(), "Сложность тренировки")
        "Equipment necessity".addString().localize(RUSSIAN(), "Необходимость оборудования")

        "Cardio".addString().localize(RUSSIAN(), "Кардио")
        "Strength".addString().localize(RUSSIAN(), "Сила")
        "Health".addString().localize(RUSSIAN(), "Здоровье")
        "Home".addString().localize(RUSSIAN(), "Дом")
        "Street".addString().localize(RUSSIAN(), "Улица")
        "Gym".addString().localize(RUSSIAN(), "Зал")
        "Easy".addString().localize(RUSSIAN(), "Просто")
        "Average".addString().localize(RUSSIAN(), "Средне")
        "Hard".addString().localize(RUSSIAN(), "Сложно")
        "Required".addString().localize(RUSSIAN(), "Необходимо")
        "Not required".addString().localize(RUSSIAN(), "Не нужно")
        "times".addString().localize(RUSSIAN(), "раз")
        "meter".addString().localize(RUSSIAN(), "метров")
        "kg".addString().localize(RUSSIAN(), "кг")
        "sec".addString().localize(RUSSIAN(), "сек")

        initMuscle()
        initFilter()
        initGoal()
        initMeasureTypes()
        initEquipments()

        initExercises()
        initWorkout()
        initWorkoutProgram()
//        userRepository
//            .save(User("somenae", workoutProgramRepository
//                .findAll(QWorkoutProgram.workoutProgram.common.name.contains(Localizable(string = "Bulging biceps and triceps program")))
//                .toMutableList()))

        println("<---- INITIALIZED ----->")

    }

    private fun initFilter() {
    }

    private fun initMuscle() {
        musclesRepository.save(Muscle(
            id = "chest",
            name = "Chest".addString().localize(RUSSIAN(), "Грудь")
        ))

        musclesRepository.save(Muscle(
            id = "quadriceps",
            name = "Quadriceps".addString().localize(RUSSIAN(), "Квадрицепс")
        ))
        musclesRepository.save(Muscle(
            id = "calf",
            name = "Calf".addString().localize(RUSSIAN(), "Икры")
        ))
        musclesRepository.save(Muscle(
            id = "triceps",
            name = "Triceps".addString().localize(RUSSIAN(), "Трицепс")
        ))
        musclesRepository.save(Muscle(
            id = "biceps",
            name = "Biceps".addString().localize(RUSSIAN(), "Бицепс")
        ))
        musclesRepository.save(Muscle(
            id = "back",
            name = "Back".addString().localize(RUSSIAN(), "Сапина")
        ))
    }

    private fun initGoal() {
        goalRepository.save(Goal(
            id = "mass_gain",
            name = "Gaining mass".addString().localize(RUSSIAN(), "Набор массы")
        ))
        goalRepository.save(Goal(
            id = "lose_wait",
            name = "Loosing weight".addString().localize(RUSSIAN(), "Сбрасывание веса")
        ))
        goalRepository.save(Goal(
            id = "good_shape",
            name = "Staying in good shape".addString().localize(RUSSIAN(), "Поддержание формы")
        ))
    }

    private fun initMeasureTypes() {
//        measureTypeRepository.deleteAll()
        measureTypeRepository.save(MeasureType(
            id = "distance",
            name = "Distance".addString().localize(RUSSIAN(), "Дистанция"),
            measurePrefix = MeasurePrefix.METER.prefix,
            measurePrefixLocalized = MeasurePrefix.METER.prefix,
        ))
        measureTypeRepository.save(MeasureType(
            id = "repetition",
            name = "Repetitions".addString().localize(RUSSIAN(), "Повторения"),
            measurePrefix = MeasurePrefix.COUNT.prefix,
                measurePrefixLocalized = MeasurePrefix.COUNT.prefix
        ))
        measureTypeRepository.save(MeasureType(
            id = "repetition_left",
            name = "Repetitions left".addString().localize(RUSSIAN(), "Повторения лево"),
            measurePrefix = MeasurePrefix.COUNT.prefix,
            measurePrefixLocalized = MeasurePrefix.COUNT.prefix,
        ))
        measureTypeRepository.save(MeasureType(
            id = "repetition_right",
            name = "Repetitions right".addString().localize(RUSSIAN(), "Повторения право"),
            measurePrefix = MeasurePrefix.COUNT.prefix,
            measurePrefixLocalized = MeasurePrefix.COUNT.prefix,
        ))
        measureTypeRepository.save(MeasureType(
            id = "weight",
            name = "Weight".addString().localize(RUSSIAN(), "Вес"),
            measurePrefix = MeasurePrefix.KILO.prefix,
            measurePrefixLocalized = MeasurePrefix.KILO.prefix,
        ))
        measureTypeRepository.save(MeasureType(
            id = "time",
            name = "Time".addString().localize(RUSSIAN(), "Время"),
            measurePrefix = MeasurePrefix.SEC.prefix,
            measurePrefixLocalized = MeasurePrefix.SEC.prefix,
        ))
    }

    private fun initExercises() {
        exerciseService.saveExercise {
            id("push_up_classical")
            name("Push ups (classic)".addString().localize(RUSSIAN(), "Отжимания (классические)"))
            detailDescription(("An exercise in which a person lying face down, with the hands under the shoulders, " +
                "raises the torso and, often, the knees off the ground by pushing down with the palms: push-ups " +
                "are usually done in a series by alternately straightening and bending the arms").addString())

            images {
                ImageBundle("https://hnet.com/video-to-gif/download/20210512-18-x0yZH700He4LijuI-6kV8iT/Hnet-image.gif")
            }
            additionalInfo {
                targetMuscle(listOf(musclesRepository.findById("triceps").get(), musclesRepository.findById("chest").get()))
                involvedMuscles(listOf(musclesRepository.findById("triceps").get(), musclesRepository.findById("chest").get()))
                addEquipment(equipmentService.findById(id = "dumbbell"))
            }
        }
        exerciseService.saveExercise {
            id("bench_press")
            name("Bench press".addString().localize(RUSSIAN(), "Жим лежа"))
            detailDescription(("An exercise in which a person lying face down, with the hands under the shoulders, " +
                "raises the torso and, often, the knees off the ground by pushing down with the palms: push-ups " +
                "are usually done in a series by alternately straightening and bending the arms").addString())
            video { Video("https://s3.amazonaws.com/onlinegym.space/M/IMG_6905.mp4") }
            images {
                ImageBundle("https://www.fewskills.com/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2019/06/image2-2.jpg.webp")
            }
            additionalInfo {
                addEquipment(equipmentService.findById(id = "barbell"))
            }
        }

        exerciseService.saveExercise {
            id("pull_up_classical")
            name("Pull ups (classic)".addString().localize(RUSSIAN(), "Подтягивания (классические)"))

            detailDescription(("Pull ups description").addString())
            images {
                ImageBundle("https://www.stepandstep.ru/wp-content/uploads/kak-nauchitsya-podtyagivatsya-na-turnike.jpg")
            }
            additionalInfo {
                targetMuscle(listOf(musclesRepository.findById("biceps").get(), musclesRepository.findById("back").get()))
                involvedMuscles(listOf(musclesRepository.findById("biceps").get(), musclesRepository.findById("back").get()))
                addEquipment(equipmentService.findById(id = "barbell"))
            }
        }

        exerciseService.saveExercise {
            id("jumping_for_time")
            name("Jumping for time".addString().localize(RUSSIAN(), "Прыжки по времени"))

            detailDescription(("Jumping for time").addString())
            images {
                ImageBundle("https://www.stepandstep.ru/wp-content/uploads/kak-nauchitsya-podtyagivatsya-na-turnike.jpg")
            }
            additionalInfo {
                targetMuscle(listOf(musclesRepository.findById("quadriceps").get()))
                involvedMuscles(listOf(musclesRepository.findById("quadriceps").get()))
            }
        }
    }

    private fun initWorkout() {
        val workoutTemplate: Workout = workout {
            id("chest_workout")
            name("Gaining push ups".addString().localize(RUSSIAN(), "Наращивание отжиманий"))
            images {
                ImageBundle(
                    "https://images.unsplash.com/photo-1519741347686-c1e0aadf4611?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2400&q=80",
                    "https://images.unsplash.com/photo-1519741347686-c1e0aadf4611?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2400&q=80"
                )
            }
            characteristics {
                type(WorkoutType.FilterValue.STRENGTH)
                difficulty(WorkoutDifficulty.FilterValue.AVERAGE)
                workoutPlaces(listOf(WorkoutPlace.FilterValue.GYM, WorkoutPlace.FilterValue.STREET))
                equipmentNecessity(EquipmentNecessity.FilterValue.NOT_REQUIRED)
            }
            description(("An exercise in which... a person lying face down, with the hands under the shoulders, raises the torso and, often, " +
                "the knees off the ground by pushing down with the palms: push-ups are usually done in a series by alternately straightening " +
                "and bending the arms").addString())
            addPhase {
                name("Push ups")
                exercises(listOf("push_up_classical", "push_up_classical", "push_up_classical", "push_up_classical"))
            }
            addPhase {
                name("Bench preses")
                exercises(listOf("bench_press", "bench_press", "bench_press", "bench_press"))
            }
            addPhase {
                name("Jumping")
                exercises(listOf("jumping_for_time", "jumping_for_time", "jumping_for_time", "jumping_for_time"))
            }
            duration(2700)
        }
        workoutService.saveWorkout(DEFAULT_LANGUAGE, workoutTemplate)
        workoutService.saveWorkout(DEFAULT_LANGUAGE, workoutTemplate.copy(id = "${workoutTemplate.id}_1"))
    }

    private fun initWorkoutProgram() {
        workoutProgramService.saveWorkoutProgram {
            id("classic_workout_program_for_mass_gain")
            name("Classic workout for mass gain Classic workout for mass gain".addString().localize(RUSSIAN(), "Обычная тренировка для набора массы"))
            description("If you start training under this program, then after".addString()
                .localize(RUSSIAN(), "Если вы начнете тренироваться по этой программе, "))
            goal(goalRepository.findAll().find { it.id == "mass_gain" }!!)
            images {
                ImageBundle(
                    "https://cdn1.coachmag.co.uk/sites/coachmag/files/styles/16x9_746/public/2016/11/chest-workout-4-weeks.jpg?itok=xp4E5DHC&timestamp=1478613470",
                    "https://cdn1.coachmag.co.uk/sites/coachmag/files/styles/16x9_746/public/2016/11/chest-workout-4-weeks.jpg?itok=xp4E5DHC&timestamp=1478613470"
                )
            }
            characteristics {
                type(WorkoutType.FilterValue.STRENGTH)
                difficulty(WorkoutDifficulty.FilterValue.AVERAGE)
                equipmentNecessity(EquipmentNecessity.FilterValue.REQUIRED)
            }
            addWeek {
                addDay {
                    dayIndex(1)
                    addScheduledWorkout {
                        id("classic_week1_day1")
                        name("Classic workout for mass gain day 1 (Week 1)".addString()
                            .localize(RUSSIAN(), "Классическая териновка для набора массы день 1 (Неделя 1)"))
                        workout("chest_workout")
                        workoutProgram("classic_workout_program_for_mass_gain")
                        addWorkoutPhase {
                            name("Push ups".addString().localize(RUSSIAN(), "Отжимания"))
                            addWorkoutSet {
                                exercise("push_up_classical")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 5.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("push_up_classical")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 6.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("push_up_classical")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 7.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("push_up_classical")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 7.toDouble(), measureTypeId = "repetition")))
                            }
                        }
                        addWorkoutPhase {
                            name("Bench presses".addString().localize(RUSSIAN(), "Жим лежа"))
                            addWorkoutSet {
                                exercise("bench_press")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 80.toDouble(), measureTypeId = "weight"),
                                    Measure(expectedValue = 10.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("bench_press")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 80.toDouble(), measureTypeId = "weight"),
                                    Measure(expectedValue = 10.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("bench_press")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 80.toDouble(), measureTypeId = "weight"),
                                    Measure(expectedValue = 10.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("bench_press")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 80.toDouble(), measureTypeId = "weight"),
                                    Measure(expectedValue = 10.toDouble(), measureTypeId = "repetition")))
                            }
                        }
                    }
                }
                addDay {
                    dayIndex(3)
                    addScheduledWorkout {
                        id("classic_week1_day1")
                        name("Classic workout for mass gain day 1 (Week 1)".addString()
                            .localize(RUSSIAN(), "Классическая териновка для набора массы день 1 (Неделя 1)"))
                        workout("chest_workout")
                        workoutProgram("classic_workout_program_for_mass_gain")
                        addWorkoutPhase {
                            name("Push ups".addString().localize(RUSSIAN(), "Отжимания"))
                            addWorkoutSet {
                                exercise("push_up_classical")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 5.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("push_up_classical")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 6.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("push_up_classical")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 7.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("push_up_classical")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 7.toDouble(), measureTypeId = "repetition")))
                            }
                        }
                        addWorkoutPhase {
                            name("Bench presses".addString().localize(RUSSIAN(), "Жим лежа"))
                            addWorkoutSet {
                                exercise("bench_press")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 80.toDouble(), measureTypeId = "weight"),
                                    Measure(expectedValue = 10.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("bench_press")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 80.toDouble(), measureTypeId = "weight"),
                                    Measure(expectedValue = 10.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("bench_press")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 80.toDouble(), measureTypeId = "weight"),
                                    Measure(expectedValue = 10.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("bench_press")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 80.toDouble(), measureTypeId = "weight"),
                                    Measure(expectedValue = 10.toDouble(), measureTypeId = "repetition")))
                            }
                        }
                        addWorkoutPhase {
                            name("Jumping for time".addString().localize(RUSSIAN(), "Прыжки на время"))
                            addWorkoutSet {
                                exercise("jumping_for_time")
                                scheduledWorkoutId("jumping_for_time_day1")
                                measures(listOf(Measure(expectedValue = 30.toDouble(), measureTypeId = "time")))
                            }
                            addWorkoutSet {
                                exercise("jumping_for_time")
                                scheduledWorkoutId("jumping_for_time_day1")
                                measures(listOf(Measure(expectedValue = 30.toDouble(), measureTypeId = "time")))
                            }
                            addWorkoutSet {
                                exercise("jumping_for_time")
                                scheduledWorkoutId("jumping_for_time_day1")
                                measures(listOf(Measure(expectedValue = 30.toDouble(), measureTypeId = "time")))
                            }
                            addWorkoutSet {
                                exercise("jumping_for_time")
                                scheduledWorkoutId("jumping_for_time_day1")
                                measures(listOf(Measure(expectedValue = 30.toDouble(), measureTypeId = "time")))
                            }
                        }
                    }
                }
                addDay {
                    dayIndex(5)
                    addScheduledWorkout {
                        id("classic_week1_day1")
                        name("Classic workout for mass gain day 1 (Week 1)".addString()
                            .localize(RUSSIAN(), "Классическая териновка для набора массы день 1 (Неделя 1)"))
                        workout("chest_workout")
                        workoutProgram("classic_workout_program_for_mass_gain")
                        addWorkoutPhase {
                            name("Push ups".addString().localize(RUSSIAN(), "Отжимания"))
                            addWorkoutSet {
                                exercise("push_up_classical")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 5.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("push_up_classical")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 6.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("push_up_classical")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 7.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("push_up_classical")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 7.toDouble(), measureTypeId = "repetition")))
                            }
                        }
                        addWorkoutPhase {
                            name("Bench presses".addString().localize(RUSSIAN(), "Жим лежа"))
                            addWorkoutSet {
                                exercise("bench_press")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 80.toDouble(), measureTypeId = "weight"),
                                    Measure(expectedValue = 10.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("bench_press")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 80.toDouble(), measureTypeId = "weight"),
                                    Measure(expectedValue = 10.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("bench_press")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 80.toDouble(), measureTypeId = "weight"),
                                    Measure(expectedValue = 10.toDouble(), measureTypeId = "repetition")))
                            }
                            addWorkoutSet {
                                exercise("bench_press")
                                scheduledWorkoutId("classic_wp_mass_gain_week1_day1")
                                measures(listOf(Measure(expectedValue = 80.toDouble(), measureTypeId = "weight"),
                                    Measure(expectedValue = 10.toDouble(), measureTypeId = "repetition")))
                            }
                        }
                    }
                }
            }
        }
    }

    private fun initEquipments() {
        equipmentService.saveEquipment {
            id("dumbbell")
            name("Dumbbell".addString().localize(RUSSIAN(), "Гантели"))
        }
        equipmentService.saveEquipment {
            id("barbell")
            name("Barbell".addString().localize(RUSSIAN(), "Штанга"))
        }
        equipmentService.saveEquipment {
            id("curl_barbell")
            name("Curl barbell".addString().localize(RUSSIAN(), "Штанга с кривым грифом"))
        }
        equipmentService.saveEquipment {
            id("pull_up_bar")
            name("Pull-up Bar".addString().localize(RUSSIAN(), "Турник для подтягивания"))
        }
        equipmentService.saveEquipment {
            id("stationary_bicycle")
            name("Stationary Bicycle".addString().localize(RUSSIAN(), "Велотеноожер"))
        }
        equipmentService.saveEquipment {
            id("rowing_machine")
            name("Rowing Machine".addString().localize(RUSSIAN(), "Гребной тренажер"))
        }
        equipmentService.saveEquipment {
            id("squat_rack")
            name("Squat rack".addString().localize(RUSSIAN(), "Тренажер для рписеданий"))
        }
        equipmentService.saveEquipment {
            id("bench_press")
            name("Bench press".addString().localize(RUSSIAN(), "Скамья ддля пресса"))
        }
        equipmentService.saveEquipment {
            id("dipping_bars")
            name("Dipping bars".addString().localize(RUSSIAN(), "Брусья"))
        }
        equipmentService.saveEquipment {
            id("peck_deck_machine")
            name("Peck deck machine".addString().localize(RUSSIAN(), "Тренажер для сведения рук"))
        }
        equipmentService.saveEquipment {
            id("hammer_strength_machine")
            name("Hammer strength machine".addString().localize(RUSSIAN(), "Тренажер хаммер"))
        }
        equipmentService.saveEquipment {
            id("lat_pull_down_machine")
            name("Lat pull down machine".addString().localize(RUSSIAN(), "Тренажер для вехней тяги"))
        }
        equipmentService.saveEquipment {
            id("scott_machine")
            name("Scott machine".addString().localize(RUSSIAN(), "Тренажер Скотта"))
        }
        equipmentService.saveEquipment {
            id("cable_crossover_machine")
            name("Cable crossover machine".addString().localize(RUSSIAN(), "Тренажер для перекрестного сведения рук"))
        }
        equipmentService.saveEquipment {
            id("triceps_extension_machine")
            name("Triceps extension machine".addString().localize(RUSSIAN(), "Тренажер трицепса"))
        }
    }
}
