package com.zemaprod.workout.controller

import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.zemaprod.workout.configuration.OAuth2ResourceServerSecurityConfiguration
import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import com.zemaprod.workout.model.workout.WorkoutProgram
import com.zemaprod.workout.service.FilterService
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@WebMvcTest(FilterController::class)
@Import(OAuth2ResourceServerSecurityConfiguration::class)
@WithMockUser
class FilterControllerTest {
    @MockBean
    private lateinit var filterService: FilterService
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    fun `should return status 200 and filter for workout program characteristic`() {

        this.mockMvc.perform(MockMvcRequestBuilders
            .get("/filter/workout_program")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
        verify(filterService, times(1)).getAllFilterForEntity(DEFAULT_LANGUAGE, WorkoutProgram::class)
    }
}
