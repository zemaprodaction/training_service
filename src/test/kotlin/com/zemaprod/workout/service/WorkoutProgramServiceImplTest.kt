package com.zemaprod.workout.service

import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class WorkoutProgramServiceImplTest : BaseIntegrationTest() {

    @Test
    fun findById() {
        val workoutProgramForSave = genericWorkoutProgram()
        workoutProgramService.saveWorkoutProgram(DEFAULT_LANGUAGE, workoutProgramForSave)
        val workoutProgramFromDb = workoutProgramService.findById(id = workoutProgramForSave.id)
        assertThat(workoutProgramFromDb.characteristic.workoutPlaces.value).contains("Home")
    }
}
