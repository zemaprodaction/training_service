package com.zemaprod.workout.service

import com.zemaprod.workout.model.ImageBundle
import com.zemaprod.workout.model.ScheduledDay
import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import com.zemaprod.workout.model.shared.Goal
import com.zemaprod.workout.model.workout.ScheduledWorkout
import com.zemaprod.workout.model.workout.WorkoutProgram
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.springframework.beans.factory.annotation.Autowired
import java.security.SecureRandom
import java.time.LocalDate

internal class SchedulerServiceImplTest {

    private var schedulerService: SchedulerService = SchedulerServiceImpl()

    @Test
    fun `should create scheduler with data depend on day index`() {
        val workoutProgram = genericWorkoutProgram()
        val scheduler: Map<LocalDate, ScheduledDay> = schedulerService.createSchedulerByWorkoutProgram(workoutProgram)
        Assertions.assertThat(scheduler.containsKey(LocalDate.now())).isTrue
        Assertions.assertThat(scheduler.containsKey(LocalDate.now().plusDays(2))).isTrue
        Assertions.assertThat(scheduler.containsKey(LocalDate.now().plusDays(6))).isTrue
        Assertions.assertThat(scheduler.containsKey(LocalDate.now().plusDays(7))).isTrue
        Assertions.assertThat(scheduler.containsKey(LocalDate.now().plusDays(9))).isTrue
        Assertions.assertThat(scheduler.containsKey(LocalDate.now().plusDays(13))).isTrue
    }

    @Test
    fun `should create scheduler with data depend on day index when index start from 3`() {
        val workoutProgram = genericWorkoutProgram2()
        val scheduler: Map<LocalDate, ScheduledDay> = schedulerService.createSchedulerByWorkoutProgram(workoutProgram)
        Assertions.assertThat(scheduler.containsKey(LocalDate.now())).isTrue
        Assertions.assertThat(scheduler.containsKey(LocalDate.now().plusDays(2))).isTrue
        Assertions.assertThat(scheduler.containsKey(LocalDate.now().plusDays(3))).isTrue
        Assertions.assertThat(scheduler.containsKey(LocalDate.now().plusDays(8))).isTrue
        Assertions.assertThat(scheduler.containsKey(LocalDate.now().plusDays(9))).isTrue
        Assertions.assertThat(scheduler.containsKey(LocalDate.now().plusDays(11))).isTrue
    }

    @Test
    fun `should create scheduler with current day when workout program has one scheduler workout`() {
        val workoutProgram = genericWorkoutProgram2()
        val scheduler: Map<LocalDate, ScheduledDay> = schedulerService.createSchedulerByWorkoutProgram(workoutProgram)
        Assertions.assertThat(scheduler.containsKey(LocalDate.now())).isTrue
    }

    val genericWorkoutProgram: () -> WorkoutProgram = {
        WorkoutProgram.workoutProgram {
            id(randomString(10))
            name(randomString(10))
            description(randomString(200))
            goal(Goal(id = randomString(10), name = randomString(20)))
            images { ImageBundle(randomString(10), randomString(10)) }
            addWeek {
                addDay {
                    dayIndex(1)
                    addScheduledWorkout {
                        id("workout_scheduler_1")
                    }
                }
                addDay {
                    dayIndex(3)
                    addScheduledWorkout {
                        id("workout_scheduler_2")
                    }
                }
                addDay {
                    dayIndex(7)
                    addScheduledWorkout {
                        id("workout_scheduler_3")
                    }
                }
            }
            addWeek {
                addDay {
                    dayIndex(1)
                    addScheduledWorkout {
                        id("workout_scheduler_3")
                    }
                }
                addDay {
                    dayIndex(3)
                    addScheduledWorkout {
                        id("workout_scheduler_4")
                    }
                }
                addDay {
                    dayIndex(7)
                    addScheduledWorkout {
                        id("workout_scheduler_5")
                    }
                }
            }
        }
    }

    val genericWorkoutProgram2: () -> WorkoutProgram = {
        WorkoutProgram.workoutProgram {
            id(randomString(10))
            name(randomString(10))
            description(randomString(200))
            goal(Goal(id = randomString(10), name = randomString(20)))
            images { ImageBundle(randomString(10), randomString(10)) }
            addWeek {
                addDay {
                    dayIndex(3)
                    addScheduledWorkout {
                        id("workout_scheduler_1")
                    }
                }
                addDay {
                    dayIndex(5)
                    addScheduledWorkout {
                        id("workout_scheduler_2")
                    }
                }
                addDay {
                    dayIndex(6)
                    addScheduledWorkout {
                        id("workout_scheduler_3")
                    }
                }
            }
            addWeek {
                addDay {
                    dayIndex(4)
                    addScheduledWorkout {
                        id("workout_scheduler_3")
                    }
                }
                addDay {
                    dayIndex(5)
                    addScheduledWorkout {
                        id("workout_scheduler_4")
                    }
                }
                addDay {
                    dayIndex(7)
                    addScheduledWorkout {
                        id("workout_scheduler_5")
                    }
                }
            }
        }
    }


    protected fun randomString(len: Int, chars: String = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"): String {
        val sb = StringBuilder(len)
        for (i in 0 until len)
            sb.append(chars[SecureRandom().nextInt(chars.length)])
        return sb.toString()
    }
}
