package com.zemaprod.workout.service

import com.zemaprod.workout.CustomCommandLineRunner
import com.zemaprod.workout.model.ImageBundle
import com.zemaprod.workout.model.entity_builder.ScheduledWorkoutBuilder
import com.zemaprod.workout.model.entity_builder.WorkoutProgramBuilder
import com.zemaprod.workout.model.exercise.Exercise
import com.zemaprod.workout.model.exercise.ExerciseType
import com.zemaprod.workout.model.exercise.Muscle
import com.zemaprod.workout.model.exercise.measure.Measure
import com.zemaprod.workout.model.filter.EquipmentNecessity
import com.zemaprod.workout.model.filter.WorkoutDifficulty
import com.zemaprod.workout.model.filter.WorkoutPlace
import com.zemaprod.workout.model.filter.WorkoutType
import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import com.zemaprod.workout.model.shared.Goal
import com.zemaprod.workout.model.workout.Workout
import com.zemaprod.workout.model.workout.WorkoutProgram
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import java.security.Principal
import java.security.SecureRandom

@DataMongoTest
@ComponentScan("com.zemaprod.workout.service", "com.zemaprod.workout.repository")
@Import(CustomCommandLineRunner::class)
abstract class BaseIntegrationTest {

    @Autowired
    protected lateinit var workoutService: WorkoutService

    @Autowired
    protected lateinit var workoutProgramService: WorkoutProgramService

    @Autowired
    protected lateinit var goalService: GoalService

    @Autowired
    protected lateinit var exerciseService: ExerciseService

    @Autowired
    protected lateinit var localizationService: LocalizationService

    @Autowired
    protected lateinit var equipmentService: EquipmentService

    @Autowired
    protected lateinit var muscleService: MuscleService


    protected val genericWorkout: () -> Workout = {
        Workout.workout {
            id(randomString(10))
            name(randomString(10))
            images { ImageBundle(randomString(10), randomString(10)) }
            description(randomString(100))
            characteristics {
                type(WorkoutType.FilterValue.STRENGTH)
                difficulty(WorkoutDifficulty.FilterValue.AVERAGE)
                workoutPlaces(listOf(WorkoutPlace.FilterValue.GYM, WorkoutPlace.FilterValue.STREET, WorkoutPlace.FilterValue.HOME))
                equipmentNecessity(EquipmentNecessity.FilterValue.NOT_REQUIRED)
            }
            addPhase {
                name(randomString(10))
                exercises(listOf(exerciseService.saveExercise {
                    id("Custom exercise ${randomString(10)}")
                }))
            }
            duration(2700)
        }
    }

    val genericWorkoutProgram: () -> WorkoutProgram = {
        WorkoutProgram.workoutProgram {
            id(randomString(10))
            name(randomString(10))
            description(randomString(200))
            goal(goalService.saveGoal(goal = Goal(id = randomString(10), name = randomString(20))))
            images { ImageBundle(randomString(10), randomString(10)) }
            addWeek {
                addDay {
                    dayIndex(1)
                    addScheduledWorkout {
                        scheduledWorkoutBuilder(this@workoutProgram)
                    }
                }
                addDay {
                    dayIndex(3)
                    addScheduledWorkout {
                        scheduledWorkoutBuilder(this@workoutProgram)
                    }
                }
                addDay {
                    dayIndex(7)
                    addScheduledWorkout {
                        scheduledWorkoutBuilder(this@workoutProgram)
                    }
                }
            }
            addWeek {
                addDay {
                    dayIndex(1)
                    addScheduledWorkout {
                        scheduledWorkoutBuilder(this@workoutProgram)
                    }
                }
                addDay {
                    dayIndex(3)
                    addScheduledWorkout {
                        scheduledWorkoutBuilder(this@workoutProgram)
                    }
                }
                addDay {
                    dayIndex(7)
                    addScheduledWorkout {
                        scheduledWorkoutBuilder(this@workoutProgram)
                    }
                }
            }
        }
    }

    private fun ScheduledWorkoutBuilder.scheduledWorkoutBuilder(workoutProgramBuilder: WorkoutProgramBuilder): ScheduledWorkoutBuilder {
        id(randomString(10))
        name(randomString(10))
        workout(workoutService.saveWorkout(workout = genericWorkout()))
        workoutProgram(workoutProgramBuilder.program.id)
        return addWorkoutPhase {
            name(randomString(10))
            addWorkoutSet {
                exercise(
                    exerciseService.saveExercise {
                        id(randomString(10))
                        name(randomString(10))
                    }
                )
                scheduledWorkoutId(this@scheduledWorkoutBuilder.scheduledWorkout.id)
                measures(listOf(Measure(expectedValue = 5.toDouble(), measureTypeId = "repetition_count")))
            }
            addWorkoutSet {
                exercise(
                    exerciseService.saveExercise {
                        id(randomString(10))
                        name(randomString(10))
                    }
                )
                scheduledWorkoutId(this@scheduledWorkoutBuilder.scheduledWorkout.id)
                measures(listOf(Measure(expectedValue = 6.toDouble(), measureTypeId = "repetition_count")))
            }
            addWorkoutSet {
                exercise(
                    exerciseService.saveExercise {
                        id(randomString(10))
                        name(randomString(10))
                    }
                )
                scheduledWorkoutId(this@scheduledWorkoutBuilder.scheduledWorkout.id)
                measures(listOf(Measure(expectedValue = 7.toDouble(), measureTypeId = "repetition_count")))
            }
        }
    }

    protected val genericExercise: () -> Exercise = {
        val muscle = muscleService.save(DEFAULT_LANGUAGE, Muscle("LEGS"))
        Exercise.exercise {
            id(randomString(10))
            name(randomString(10))
            exerciseType(ExerciseType.CARDIO)
            images { ImageBundle(randomString(10), randomString(10)) }
            detailDescription(randomString(10))
            additionalInfo {
                targetMuscle(listOf(muscleService.findById(muscle.id)))
                involvedMuscles(listOf(muscleService.findById(muscle.id)))
                addEquipment(equipmentService.findAll(DEFAULT_LANGUAGE).first())
                addEquipment(equipmentService.findAll(DEFAULT_LANGUAGE).last())
            }
        }
    }

    @BeforeEach
    fun setUp() {
        equipmentService.saveEquipment(DEFAULT_LANGUAGE) {
            name("Test equipment")
        }
        equipmentService.saveEquipment(DEFAULT_LANGUAGE) {
            name("Test equipment2")
        }
    }

    protected fun randomString(len: Int, chars: String = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"): String {
        val sb = StringBuilder(len)
        for (i in 0 until len)
            sb.append(chars[SecureRandom().nextInt(chars.length)])
        return sb.toString()
    }

    inner class TestUser(private val userName: String = "TestUser${randomString(10)}") : Principal {
        override fun getName(): String {
            return userName
        }
    }
}
