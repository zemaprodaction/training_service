package com.zemaprod.workout.service

import com.zemaprod.workout.model.filter.Filter
import com.zemaprod.workout.model.filter.FilterDefinition
import com.zemaprod.workout.model.filter.WorkoutPropertyType
import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import com.zemaprod.workout.model.workout.Workout
import com.zemaprod.workout.model.workout.WorkoutProgram
import com.zemaprod.workout.repository.FilterRepository
import javassist.NotFoundException
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.reflect.jvm.jvmName

class FilterServiceTest : BaseIntegrationTest() {

    @Autowired
    protected lateinit var filterService: FilterService

    @Autowired
    protected lateinit var filterRepository: FilterRepository

    @Test
    fun `should return list of filter definition after start application`() {
        val filters = filterRepository.findFilterDefinitionByClass(WorkoutProgram::class)
        assertThatWorkoutProgramFilterListHasRequredFilterDefinition(filters)
    }

    @Test
    fun `should return all filter for workout program entity`() {
        val filters = filterService.getAllFilterForEntity(DEFAULT_LANGUAGE, WorkoutProgram::class)
        Assertions.assertThat(filters)
            .contains(Filter(WorkoutPropertyType.WORKOUT_TYPE.id, WorkoutPropertyType.WORKOUT_TYPE.defaultName,
                "workoutType", listOf("Cardio", "Strength", "Health"), listOf("Cardio", "Strength", "Health"),true),
                Filter(WorkoutPropertyType.WORKOUT_PLACE.id, WorkoutPropertyType.WORKOUT_PLACE.defaultName,
                    "workoutPlaces", listOf("Home", "Street", "Gym"), listOf("Home", "Street", "Gym"),false),
                Filter(WorkoutPropertyType.WORKOUT_DIFFICULTY.id, WorkoutPropertyType.WORKOUT_DIFFICULTY.defaultName,
                    "workoutDifficulty", listOf("Easy", "Average", "Hard"), listOf("Easy", "Average", "Hard"),true),
                Filter(WorkoutPropertyType.EQUIPMENT_NECESSITY.id, WorkoutPropertyType.EQUIPMENT_NECESSITY.defaultName,
                    "equipmentNecessity", listOf("Required", "Not required"), listOf("Required", "Not required"),true))
    }

    private fun assertThatWorkoutProgramFilterListHasRequredFilterDefinition(filters: List<FilterDefinition>) {
        Assertions.assertThat(filters)
            .contains(FilterDefinition(WorkoutPropertyType.WORKOUT_TYPE.id, WorkoutPropertyType.WORKOUT_TYPE.defaultName, "workoutType", true),
                FilterDefinition(WorkoutPropertyType.WORKOUT_PLACE.id, WorkoutPropertyType.WORKOUT_PLACE.defaultName, "workoutPlaces", false),
                FilterDefinition(WorkoutPropertyType.WORKOUT_DIFFICULTY.id, WorkoutPropertyType.WORKOUT_DIFFICULTY.defaultName, "workoutDifficulty", true),
                FilterDefinition(WorkoutPropertyType.EQUIPMENT_NECESSITY.id, WorkoutPropertyType.EQUIPMENT_NECESSITY.defaultName, "equipmentNecessity", true))
    }
}
