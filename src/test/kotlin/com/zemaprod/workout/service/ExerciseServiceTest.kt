package com.zemaprod.workout.service

import com.zemaprod.workout.model.equipment.Equipment
import com.zemaprod.workout.model.equipment.Equipment.Companion.equipment
import com.zemaprod.workout.model.exercise.Exercise.Companion.exercise
import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import com.zemaprod.workout.utils.exeption.WorkoutServiceException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import java.util.*

class ExerciseServiceTest : BaseIntegrationTest() {

    @Test
    fun `assert that exercise can be saved`() {
        val exerciseToSave = genericExercise()
        val saved = exerciseService.saveExercise(DEFAULT_LANGUAGE, exerciseToSave)
        assertThat(saved)
            .isEqualTo(exerciseToSave)
    }

    @Test
    fun `assert that exercise can be retrieved by id`() {
        val exerciseToSave = genericExercise()
        exerciseService.apply {
            saveExercise(DEFAULT_LANGUAGE, exerciseToSave)
            assertThat(findById(DEFAULT_LANGUAGE, exerciseToSave.id))
                .isEqualTo(exerciseToSave)

            assertThat(findByIdIsIn(DEFAULT_LANGUAGE, setOf(exerciseToSave.id)))
                .anyMatch { it == exerciseToSave }
        }
    }

    @Test
    fun `assert that exercise can be retrieved via find all`() {
        val exerciseToSave = genericExercise()
        exerciseService.apply {
            saveExercise(DEFAULT_LANGUAGE, exerciseToSave)
            assertThat(findAllExercises(DEFAULT_LANGUAGE))
                .anyMatch { it == exerciseToSave }
        }
    }

    @Test
    fun `assert that exercise can be deleted`() {
        val exerciseToSave = genericExercise()
        exerciseService.apply {
            saveExercise(DEFAULT_LANGUAGE, exerciseToSave)
            removeExercise(exerciseToSave.id)
            assertThat(findAllExercises(DEFAULT_LANGUAGE))
                .noneMatch { it == exerciseToSave }
            assertThrows(WorkoutServiceException::class.java) {
                findById(DEFAULT_LANGUAGE, exerciseToSave.id)
            }
        }
    }

    @Test
    fun `assert that correct name string will be returned if it was localized`() {
        val nameToSet = randomString(10)
        val localizedName = randomString(10)
        val exerciseToSave = exercise {
            name(nameToSet)
        }
        exerciseService.apply {
            saveExercise(DEFAULT_LANGUAGE, exerciseToSave)
            localizationService.localize(Locale.FRANCE, mapOf(nameToSet to localizedName))

            assertThat(findAllExercises(Locale.FRANCE))
                .anyMatch { it.name == localizedName }

            assertThat(findById(Locale.FRANCE, exerciseToSave.id).name)
                .isEqualTo(localizedName)

            assertThat(findByIdIsIn(Locale.FRANCE, setOf(exerciseToSave.id)).first { it.id == exerciseToSave.id }.name)
                .isEqualTo(localizedName)
        }
    }

    @Test
    fun `assert that correct detailed description string will be returned if it was localized`() {
        val descriptionToSet = randomString(10)
        val localizedDescription = randomString(10)
        val exerciseToSave = exercise {
            name(randomString(10))
            detailDescription(descriptionToSet)
        }
        exerciseService.apply {
            saveExercise(DEFAULT_LANGUAGE, exerciseToSave)
            localizationService.localize(Locale.FRANCE, mapOf(descriptionToSet to localizedDescription))

            assertThat(findAllExercises(Locale.FRANCE))
                .anyMatch { it.detailedDescription == localizedDescription }

            assertThat(findById(Locale.FRANCE, exerciseToSave.id).detailedDescription)
                .isEqualTo(localizedDescription)
        }
    }

    @Test
    fun `assert that correct equipment name string will be returned if it was localized`() {
        val equipmentNameToSet = randomString(10)
        val localizedName = randomString(10)
        val equipmentId = randomString(10)
        val equipment = equipment {
            id(equipmentId)
            name(equipmentNameToSet)
        }
        equipmentService.saveEquipment(DEFAULT_LANGUAGE, equipment)
        localizationService.localize(Locale.FRANCE, mapOf(equipmentNameToSet to localizedName))
        val exerciseToSave = exercise {
            name(randomString(10))
            additionalInfo {
                addEquipment(equipment)
            }
        }
        exerciseService.apply {
            saveExercise(DEFAULT_LANGUAGE, exerciseToSave)
            assertThat(findAllExercises(Locale.FRANCE))
                .anyMatch { it.additionalInfo.equipment.any { equipment -> equipment?.name == localizedName } }

            assertThat(findById(Locale.FRANCE, exerciseToSave.id).additionalInfo.equipment)
                .anyMatch { it?.name == localizedName }
        }
    }

    @Test
    fun `assert that adding single equipment with non-existent id will null this equipment after save`() {
        val exerciseToSave = exercise {
            name(randomString(10))
            additionalInfo {
                addEquipment(equipmentService.findAll(DEFAULT_LANGUAGE).last())
            }
        }
        exerciseService.apply {
            saveExercise(DEFAULT_LANGUAGE, exerciseToSave)

            saveExercise(DEFAULT_LANGUAGE, exerciseToSave.copy(
                additionalInfo = exerciseToSave.additionalInfo.copy(equipment = exerciseToSave.additionalInfo.equipment.map { it?.copy(id = randomString(10)) })
            ))

            assertThat(findById(DEFAULT_LANGUAGE, exerciseToSave.id).additionalInfo.equipment)
                .isNotEmpty
                .allMatch { it == null }
        }
    }

    @Test
    fun `assert that adding equipment with non-existent id to multiple exercises will make equipment list empty after save`() {
        val exerciseToSave = exercise {
            name(randomString(10))
            additionalInfo {
                addEquipment(equipmentService.findAll(DEFAULT_LANGUAGE).first())
                addEquipment(equipmentService.findAll(DEFAULT_LANGUAGE).last())
            }
        }
        exerciseService.apply {
            saveExercise(DEFAULT_LANGUAGE, exerciseToSave)

            saveExercise(DEFAULT_LANGUAGE, exerciseToSave.copy(
                additionalInfo = exerciseToSave.additionalInfo.copy(equipment = exerciseToSave.additionalInfo.equipment.map { it?.copy(id = randomString(10)) })
            ))

            assertThat(findById(DEFAULT_LANGUAGE, exerciseToSave.id).additionalInfo.equipment)
                .isEmpty()
        }
    }

    @Test
    fun `assert that only equipment id is enough to place equipment to exercise`() {
        val exerciseToSave = genericExercise()
        val equipmentId = randomString(10)
        val equipment = equipment {
            id(equipmentId)
            name(randomString(10))
        }
        equipmentService.saveEquipment(DEFAULT_LANGUAGE, equipment)

        exerciseService.apply {
            saveExercise(DEFAULT_LANGUAGE, exerciseToSave)

            saveExercise(DEFAULT_LANGUAGE, exerciseToSave) {
                additionalInfo {
                    addEquipment(Equipment(id = equipment.id))
                }
            }

            assertThat(findById(DEFAULT_LANGUAGE, exerciseToSave.id).additionalInfo.equipment)
                .anyMatch { it == equipment }
        }
    }

    @Test
    fun `assert that changes to equipment from within exercise will not be saved`() {
        val nameToChangeTo = randomString(10)
        val equipment = equipment {
            id(randomString(10))
            name(randomString(10))
        }
        equipmentService.saveEquipment(equipment = equipment)
        val exerciseToSave = exercise {
            name(randomString(10))
            additionalInfo {
                addEquipment(equipment)
            }
        }
        exerciseService.apply {
            saveExercise(DEFAULT_LANGUAGE, exerciseToSave)

            saveExercise(DEFAULT_LANGUAGE, exerciseToSave.copy(
                additionalInfo = exerciseToSave.additionalInfo.copy(equipment = exerciseToSave.additionalInfo.equipment.map { it?.copy(name = nameToChangeTo) })
            ))

            assertThat(equipmentService.findAll())
                .noneMatch { it.name == nameToChangeTo }

            assertThat(findById(DEFAULT_LANGUAGE, exerciseToSave.id).additionalInfo.equipment.first())
                .isEqualTo(equipment)
        }
    }

    @Test
    fun `assert that changes to equipment will result in changing equipment on all the exercises it belongs to`() {
        val equipment = equipment {
            id(randomString(10))
            name(randomString(10))
        }
        equipmentService.saveEquipment(equipment = equipment)
        val exerciseToSave = exercise {
            name(randomString(10))
            additionalInfo {
                addEquipment(equipment)
            }
        }
        exerciseService.apply {
            saveExercise(DEFAULT_LANGUAGE, exerciseToSave)

            val updatedEquipment = equipmentService.saveEquipment(equipment = equipment)

            assertThat(findById(DEFAULT_LANGUAGE, exerciseToSave.id).additionalInfo.equipment.first())
                .isEqualTo(updatedEquipment)
        }
    }

}
