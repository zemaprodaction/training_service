package com.zemaprod.workout.service

import com.zemaprod.workout.model.locale.DEFAULT_LANGUAGE
import com.zemaprod.workout.utils.exeption.WorkoutServiceException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import java.util.*

class LocalizationServiceTest : BaseIntegrationTest() {

    @Test
    fun `assert that after adding any number of strings the allStrings will hold all the added strings`() {
        val numberOfStringsToAdd = 10
        localizationService.apply {
            repeat((1..numberOfStringsToAdd).count()) {
                addString(DEFAULT_LANGUAGE, randomString(10))
            }
            assertThat(getAllStrings())
                .size()
                .isGreaterThanOrEqualTo(numberOfStringsToAdd)
        }
    }

    @Test
    fun `assert that if string is not available on given language - the default one will be returned`() {
        val stringToAdd = randomString(10)
        localizationService.apply {
            addString(string = stringToAdd)
            assertThat(getString(stringToAdd, Locale.FRANCE))
                .isEqualTo(stringToAdd)
        }
    }

    @Test
    fun `assert that string can be localized and retrieved for a particular language`() {
        val originalString = randomString(10)
        val localizedString = randomString(10)
        localizationService.apply {
            addString(string = originalString)
            localize(Locale.FRANCE, mapOf(originalString to localizedString))
            assertThat(getString(originalString, Locale.FRANCE))
                .isEqualTo(localizedString)
        }
    }

    @Test
    fun `assert that if no strings found for a given key - the requested string will be returned`() {
        val originalString = randomString(10)
        val stringToRequest = randomString(10)
        localizationService.apply {
            addString(string = originalString)
            assertThat(getString(stringToRequest, Locale.FRANCE))
                .isEqualTo(stringToRequest)
        }
    }

    @Test
    fun `assert that bulk localization is possible`() {
        val originalString = randomString(10)
        val firstToLocalize = Pair<Locale, String>(Locale.FRANCE, randomString(10))
        val secondToLocalize = Pair<Locale, String>(Locale.CHINA, randomString(10))
        localizationService.apply {
            addString(string = originalString)
            localize(
                mapOf(
                    originalString to mapOf(
                        firstToLocalize,
                        secondToLocalize
                    )
                )
            )
            assertThat(getString(originalString, DEFAULT_LANGUAGE))
                .isEqualTo(originalString)
            assertThat(getString(originalString, firstToLocalize.first))
                .isEqualTo(firstToLocalize.second)
            assertThat(getString(originalString, secondToLocalize.first))
                .isEqualTo(secondToLocalize.second)
        }
    }

    @Test
    fun `assert that attempt to localize to default language and different string value will result in error`() {
        val originalString = randomString(10)
        val localizedString = randomString(10)
        assertThrows(WorkoutServiceException::class.java) {
            localizationService.apply {
                addString(string = originalString)
                localize(DEFAULT_LANGUAGE, mapOf(originalString to localizedString))
            }
        }
    }

    @Test
    fun `assert that it is allowed to pass same string for default language while trying to localize`() {
        val originalString = randomString(10)
        localizationService.apply {
            addString(string = originalString)
            localize(DEFAULT_LANGUAGE, mapOf(originalString to originalString))
            assertThat(getString(originalString))
                .isEqualTo(originalString)
        }
    }

    @Test
    fun `assert that trying to localize non-existent string will result in error`() {
        val originalString = randomString(10)
        assertThrows(WorkoutServiceException::class.java) {
            localizationService.localize(Locale.FRANCE, mapOf(originalString to originalString))
        }
        assertThrows(WorkoutServiceException::class.java) {
            localizationService.localize(
                mapOf(
                    originalString to mapOf(
                        Pair<Locale, String>(Locale.FRANCE, randomString(10)),
                    )
                )
            )
        }
    }

    @Test
    fun `assert that trying to bulk localize non-existent string will add default string and then localize it`() {
        val originalString = randomString(10)
        val toLocalize = Pair<Locale, String>(Locale.FRANCE, randomString(10))
        with(localizationService) {
            localize(
                mapOf(
                    originalString to mapOf(
                        Pair(DEFAULT_LANGUAGE, originalString),
                        toLocalize,
                    )
                )
            )
            assertThat(getString(originalString, DEFAULT_LANGUAGE))
                .isEqualTo(originalString)
            assertThat(getString(originalString, toLocalize.first))
                .isEqualTo(toLocalize.second)
        }
    }

    @Test
    fun `assert that localizing string where the original string was not on Default(en) language will also create a Default(en) string entry with all the localizations`() {
        val originalString = "Original String_${randomString(3)}"
        val stringGivenToDefaultLanguage = "Default language String_${randomString(3)}"
        val localizationToSomeOtherLanguage = Pair<Locale, String>(Locale.CHINA, randomString(10))
        localizationService.apply {
            // the original localization entry is not in english
            addString(Locale.FRENCH, string = originalString)

            // localizing entry with not default original locale
            localize(
                mapOf(
                    originalString to mapOf(
                        DEFAULT_LANGUAGE to stringGivenToDefaultLanguage,
                        localizationToSomeOtherLanguage
                    )
                )
            )

            assertThat(getString(originalString, DEFAULT_LANGUAGE))
                .isEqualTo(stringGivenToDefaultLanguage)

            assertThat(getString(stringGivenToDefaultLanguage, DEFAULT_LANGUAGE))
                .isEqualTo(stringGivenToDefaultLanguage)

            assertThat(getString(originalString, Locale.FRENCH))
                .isEqualTo(originalString)

            assertThat(getString(originalString, localizationToSomeOtherLanguage.first))
                .isEqualTo(localizationToSomeOtherLanguage.second)

            assertThat(getString(stringGivenToDefaultLanguage, Locale.FRENCH))
                .isEqualTo(originalString)
        }
    }
}

