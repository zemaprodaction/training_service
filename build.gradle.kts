import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "com.zemaprod"
version = "0.4.0"

buildscript {

	extra.set("springBootVersion", "2.4.1")
	extra.set("sourceCompatibility", "11")

	repositories {
		mavenCentral()
		maven("https://repo.spring.io/milestone")
	}

	dependencies {
		classpath("org.springframework.boot:spring-boot-gradle-plugin:${project.extra["springBootVersion"]}")
		classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${project.properties["kotlinVersion"]}")
		classpath("org.jetbrains.kotlin:kotlin-allopen:${project.properties["kotlinVersion"]}")
		classpath("org.jetbrains.kotlin:kotlin-noarg:${project.properties["kotlinVersion"]}")
	}
}

repositories {
	mavenCentral()
	maven("https://repo.spring.io/snapshot")
	maven("https://repo.spring.io/milestone")
}


plugins {
	kotlin("jvm")
	kotlin("plugin.spring")
	kotlin("plugin.jpa")
	eclipse
	idea
	id("org.springframework.boot") version "2.4.1"
	id("io.spring.dependency-management") version "1.0.10.RELEASE"
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-data-mongodb")
	implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
	implementation("commons-io:commons-io:2.6")
	//support java 11
	implementation("org.glassfish.jaxb:jaxb-runtime")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.0.1")
	implementation(group = "org.modelmapper", name = "modelmapper", version = "2.3.2")
	implementation("org.jetbrains.kotlin:kotlin-stdlib:${project.properties["kotlinVersion"]}")
	runtimeOnly("org.springframework.boot:spring-boot-devtools")
	runtimeOnly("com.h2database:h2")
	developmentOnly("org.springframework.boot:spring-boot-devtools")
	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}
	testImplementation("org.springframework.security:spring-security-test")
	testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
	testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
	testImplementation("de.flapdoodle.embed:de.flapdoodle.embed.mongo")
}

tasks {
	withType<KotlinCompile> {
		kotlinOptions.jvmTarget = "${project.extra["sourceCompatibility"]}"
		kotlinOptions.freeCompilerArgs = listOf("-Xjsr305=strict", "-Xjvm-default=enable")
	}
	withType<Test> {
		useJUnitPlatform()
	}

	getByName<org.springframework.boot.gradle.tasks.bundling.BootJar>("bootJar") {
		classpath(configurations["developmentOnly"])
	}
}




